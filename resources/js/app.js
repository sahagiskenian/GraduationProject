/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


import RouterMainComponent from './components/RouterMainComponent.vue'
import VueRouter from "vue-router";
import Vuex, { mapActions } from 'vuex'
import Notifications from 'vue-notification'
import router from './router/routes'
require('vue2-animate/dist/vue2-animate.min.css')
import stores from './vuex/store';
import VModal from 'vue-js-modal/dist/index.nocss.js'
import 'vue-js-modal/dist/styles.css'
require('./bootstrap');

window.Vue = require('vue');

require("bootstrap-css-only/css/bootstrap.min.css");
require("mdbvue/lib/css/mdb.min.css");
require("@fortawesome/fontawesome-free/css/all.min.css");
/*
or for SSR:
import Notifications from 'vue-notification/dist/ssr.js'
*/


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(Notifications);
Vue.use(VModal);
const store = new Vuex.Store(stores);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 Vue.component('pagination', require('laravel-vue-pagination'));
const app = new Vue({
    el: '#app',
    router,
    mode: 'history',
    store,
    components: {
       index:RouterMainComponent
    },
});
