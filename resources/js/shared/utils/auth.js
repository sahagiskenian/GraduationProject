export function isLoggedIn(token) {
    return localStorage.getItem("isLoggedIn") == 'true';
}

export function logIn(token) {
    localStorage.setItem("isLoggedIn", true);
    localStorage.setItem("token", token);
}

export function logOut() {
    localStorage.setItem("isLoggedIn", false);
    localStorage.removeItem("token");
}
