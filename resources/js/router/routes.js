
import HomeMainComponent from '../components/Home/MainComponent.vue'
import VueRouter from "vue-router";
import UniversitiesMainComponent from '../components/Home/Universities/UniversitiesMainComponent.vue'
import CollegesMainComponent from '../components/Home/Colleges/CollegesMainComponent.vue'
import ContactComponent from '../components/Home/Contact/MainContactComponent.vue'
import MainProfileComponent from '../components/Home/Profile/MainProfileComponent.vue'
import LoginComponent from '../components/Auth/LoginComponent.vue'
import SingleTabPageComponent  from '../components/Home/Tabs/SingleTabPageComponent.vue'
const routes = [
    {
        path: "/",
        component: HomeMainComponent,
        name: "home"
    },
    {
        path: "/universities",
        component: UniversitiesMainComponent,
        name: "universities"
    },
    {
        path: "/universities/:id",
        component: CollegesMainComponent,
        name: "colleges"
    },
    {
        path: "/contactus",
        component: ContactComponent,
        name: "contact"
    },
    {
        path: "/slogin",
        component: LoginComponent,
        name: "login"
    },
    {
        path: "/college/:id",
        component: MainProfileComponent,
        name: "profile"
    },
    {
        path: "/tab/:id",
        component: SingleTabPageComponent,
        name: "tabs"
    },




];

const router = new VueRouter({
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
          return savedPosition
        } else {
          return { x: 0, y: 0 }
        }
      },

    routes, // short for `routes: routes`
    mode: "history"
});


export default router;
