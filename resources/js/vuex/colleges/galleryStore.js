

import axios from 'axios'
import { api } from '../../shared/api';


export default {


    state: {

            isloading:false,
            collegeGallery:{},
            paginatecollegeGallery:{},
            errMess:null,


    },
    mutations: {
        setCollegeGallery(state, payload) {
            state.collegeGallery = payload.data;
            state.paginatecollegeGallery = payload;
        },
        collegeGalleryLoading(state, payload) {
            state.isloading = payload;
        }
    },
    actions: {
     GetCollegeGallery({commit,state},{id,page=1}) {
        var result;
        commit('collegeGalleryLoading', true);
           const request = axios.get(api+"collegegallery/"+ id +"?page=" + page).then(response => {
                 result=response.data.data;
          console.log(result);
          commit('setCollegeGallery', result);
          commit('collegeGalleryLoading', false);
             })
            }

    },
    getters: {
        collegeGallery: (state) => state.collegeGallery,
        paginatecollegeGallery: (state) => state.paginatecollegeGallery,


    }





}
