

import axios from 'axios'
import { api } from '../../shared/api';


export default {


    state: {

            isloading:false,
            questions:{},
            errMess:null,
            paginatequestions:{},
            page:1

    },
    mutations: {
        setQuestions(state, payload) {
            state.questions = payload.data;
            state.paginatequestions = payload;
        },
        questionsLoading(state, payload) {
            state.isloading = payload;
        }
    },
    actions: {
     GetCommonQuestions({commit,state},page=1) {
         var result;
         commit('questionsLoading', true);
            const request = axios.get(api+"commonquestions?page=" + page).then(response => {
                  result=response.data.data;
           console.log(result);
           commit('setQuestions', result);
           commit('questionsLoading', false);
              })
            },


    GetFilteredCommonQuestions({commit,state},{page=1,college}) {
        var result;
        commit('questionsLoading', true);
           const request = axios.get(api+"commonquestions/"+ college +"?page=" + page).then(response => {
                 result=response.data.data;
          console.log(result);
          commit('setQuestions', result);
          commit('questionsLoading', false);
             })


   },  },
    getters: {
        questions: (state) => state.questions,
        paginatequestions: (state) => state.paginatequestions,

    }





}
