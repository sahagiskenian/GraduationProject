import LoginStore from "./auth/LoginStore";
import galleryStore from "./colleges/galleryStore";
import commonQuestionsStore from "./commonQuestions/commonQuestionsStore";
import collegeProfileStore from "./profile/collegeProfileStore";
import universitystore from "./universities/universitystore";
import TabsStore from "./Social/TabsStore.js"


export default {
    modules: {
        commonQuestions:commonQuestionsStore,
        universities:universitystore,
        collegeProfile:collegeProfileStore,
        collegeGallery:galleryStore,
        auth:LoginStore,
        tabs:TabsStore

      }

};
