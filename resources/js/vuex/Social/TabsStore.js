

import axios from 'axios'
import { api } from '../../shared/api';


export default {


    state: {

            isloading:false,
            tabs:{},
            errMess:null,


    },
    mutations: {
        setTabs(state, payload) {
            state.tabs = payload;

        },
        tabsLoading(state, payload) {
            state.isloading = payload;
        }
    },
    actions: {
     GetTabs({commit,state},page=1) {
         var result;
         commit('tabsLoading', true);
            const request = axios.get(api+"tab/get", {
  headers: {
    'Authorization': `Bearer ${localStorage.getItem('token')}`
  }
}).then(response => {
                  result=response.data.data;

           commit('setTabs', result);
           commit('tabsLoading', false);
              })
            }

    },
    getters: {
        tabs: (state) => state.tabs,


    }





}
