import { isLoggedIn, logOut ,logIn} from "../../shared/utils/auth";

export default {
    state: {
        isloading:false,
        errmess:null,
        isLoggedIn: localStorage.getItem('token') ? true : false,
        user: {},
        token:null,
    },
    mutations: {
        setLoading(state,payload){
         state.isloading=payload;
        },
        setUser(state, payload) {
            state.user = payload;
        },
        setToken(state, payload) {
            state.token = payload;
        },
        setLoggedIn(state, payload) {
            state.isLoggedIn = payload;
        }
    },
    actions: {
       registerUser({commit,state,dispatch},creds) {

              commit('setLoading',true);

            const request = axios.post("/api/register",creds).then(response => {


                logIn();
                dispatch("loadUser");

                Vue.notify({
                    group: 'register',
                    title: 'Thank You',
                    type: 'success',
                    text: "registered in successfully"
                  })

                  window.location.assign("/");


                   }).catch (error=> {
                   // this.errors = error.response && ;
                   error.response.data.errors.map(error=>
                    Vue.notify({
                        group: 'register',
                        title: 'Attention',
                        type: 'error',
                        text: error
                      })
                    )

                  });
                  commit('setLoading',false);
          },
        async loadUser({ commit, dispatch }) {

            if (isLoggedIn()) {
                try {
                    const user = (await axios.get("/api/user")).data;
                    console.log(user);
                    console.log(this);
                    commit("setUser", user);
                    commit("setLoggedIn", true);
                } catch (error) {
                    dispatch("logout");
                }
            }
        },


      async logout({commit,state,dispatch}) {


            try {

          //      const user = (await   axios.post('/api/logout')).data;

                commit("setUser", {});
                commit("setLoggedIn", false);
                logOut();
                Vue.notify({
                    group: 'foo',
                    title: 'Thank You',
                    type: 'success',
                    text: "Logged Out Sucessfully"
                  })
              window.location.push("/");
            } catch (error) {
              // dispatch("logout");
            }

    },
  async login({commit,state,dispatch},creds) {


        try {

            const user=  await axios.post("/api/login",creds );
           logIn(user.data.user.token);
           commit("setUser", user.data.user);
           commit("setToken", user.data.user.token);
           commit("setLoggedIn", true);
      //     console.log(user);
           Vue.notify({
            group: 'foo',
            title: 'Thank You',
            type: 'success',
            text: "Logged In Sucessfully"
          })
          const options = {
            headers: {'Authorization': 'Bearer '+ state.token}
          };

        const tabs=  axios.get('/api/tab/get', {
            headers: {
              'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
          })
        .then(response=>console.log(response))
        .catch(e=>console.log(e));
      //  console.log(tabs);
        } catch (error) {
            console.log(error);
            Vue.notify({
                group: 'foo',
                title: 'Thank You',
                type: 'error',
                text: "Wrong Credentials"
              })

        }

        this.loading = false;
      },
    },
    getters: {
        isAuth: (state) => state.isLoggedIn,
        
    }
};
