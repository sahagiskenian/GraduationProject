

import axios from 'axios'
import { api } from '../../shared/api';


export default {


    state: {

            isloading:false,
            collegeProfile:{},
            errMess:null,


    },
    mutations: {
        setCollegeProfile(state, payload) {
            state.collegeProfile = payload;

        },
        profileLoading(state, payload) {
            state.isloading = payload;
        }
    },
    actions: {
     GetCollegeProfile({commit,state},id) {
         var result;
         commit('profileLoading', true);
            const request = axios.get(api+"collegedetails/"+id).then(response => {
                  result=response.data.data;
           console.log(result);
           commit('setCollegeProfile', result);
           commit('profileLoading', false);
              })
            }

    },
    getters: {
        collegeProfile: (state) => state.collegeProfile,


    }





}
