

import axios from 'axios'
import { api } from '../../shared/api';


export default {


    state: {

            isloading:false,
            universities:{},
            errMess:null,


    },
    mutations: {
        setUniversities(state, payload) {
            state.universities = payload;

        },
        universitiesLoading(state, payload) {
            state.isloading = payload;
        }
    },
    actions: {
     GetUniversities({commit,state},page=1) {
         var result;
         commit('universitiesLoading', true);
            const request = axios.get(api+"universities").then(response => {
                  result=response.data.data;
           console.log(result);
           commit('setUniversities', result);
           commit('universitiesLoading', false);
              })
            }

    },
    getters: {
        universities: (state) => state.universities,


    }





}
