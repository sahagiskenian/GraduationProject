@extends('layouts.adminlayout')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">
           دفعات كلية {{Auth::user()->college->name}}
          </h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>

        <div class="content-header-right  offset-3 col-md-3 col-12">

            <button type="button" class="btn btn-outline-primary block btn-lg" data-toggle="modal"
            data-target="#default">
         اضافة دفعة
            </button>

            <!-- Modal -->
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
            aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">
                   دفعات
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                 <form action="/admin/addgeneration" method="POST">
                    @csrf
                    <fieldset class="form-group">
                        <input class="form-control" type="text" name="name" placeholder="رقم الدفعة" />
                           </fieldset>

                      <fieldset class="form-group">

                    <label>
           تاريخ البدء
                        <input type="date" class="form-control" name="entry_date"/>

                    </label>

                    </fieldset>
                    <fieldset class="form-group">

                        <label>
                            تاريخ التخرج
                                         <input type="date" class="form-control" name="graduation_date"/>

                                     </label>
                    </fieldset>
                    <fieldset class="form-group">

                        <label>
                            عدد الطلاب
                                         <input type="number" class="form-control" name="student_num"/>

                                     </label>
                    </fieldset>
                    <fieldset class="form-group">

                        <label>
                           وصف الدفعة
                                         <textarea type="text" class="form-control" name="description"></textarea>

                                     </label>
                    </fieldset>







                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">اغلاق</button>
                    <button type="submit" class="btn btn-outline-primary">اضافة</button>
                </form>
                  </div>
                </div>
              </div>
            </div>



        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>

                          <th>دفعة</th>
                          <th>كلية</th>
                          <th> تاريخ دخول </th>
                          <th> تاريخ تخرج </th>
                          <th> عدد الطلاب </th>
                          <th> حذف </th>
                        </tr>
                      </thead>
                    <tbody>
                        @foreach ($generations as $generation)
                            <tr>
 <td>{{$generation->name}}</td>
 <td>{{$generation->college->name}}</td>
 <td>{{$generation->entry_date}}</td>
 <td>{{$generation->graduation_date}}</td>
 <td>{{$generation->student_num}}</td>
<td> <a class="btn btn-danger" href="/admin/removegeneration/{{$generation->id}}">Del</a> </td>
                            </tr>
                        @endforeach

                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>


@endsection
