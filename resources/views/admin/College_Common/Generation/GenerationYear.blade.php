@extends('layouts.adminlayout')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">
            دفعة سنة
          </h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>

        <div class="content-header-right  offset-3 col-md-3 col-12">

            <button type="button" class="btn btn-outline-primary block btn-lg" data-toggle="modal"
            data-target="#default">
            ربط مواد
            </button>

            <!-- Modal -->
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
            aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">
                    مواد
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                 <form action="/admin/addgenerationyear" method="POST">
                    @csrf
                    <fieldset class="form-group">
                        <select class="form-control" name="year_id" aria-label="Default select example">
                            <option selected>   السنة </option>
                            @foreach ($years as $year)
                            <option value="{{$year->id}}"> {{$year->name}} </option>

                            @endforeach
                          </select>
                           </fieldset>

                      <fieldset class="form-group">

                        <select id="university_id" class="form-control" name="university_calender_id"  aria-label="Default select example">
                            <option selected>     تقويم الجامعي</option>
                            @foreach ($universityCalenders as $calender)
                            <option value="{{$calender->id}}"> {{$calender->year}} </option>

                            @endforeach
                          </select>
                    </fieldset>
                    <fieldset class="form-group">

                        <select id="university_id" class="form-control" name="department_id"  aria-label="Default select example">
                            <option selected>     القسم</option>
                            @foreach ($departments as $department)
                            <option value="{{$department->id}}"> {{$department->name}} </option>

                            @endforeach
                          </select>
                    </fieldset>
                    <fieldset class="form-group">

                        <select id="university_id" class="form-control" name="generation_id"  aria-label="Default select example">
                            <option selected>        الدفعة</option>
                            @foreach ($generations as $generation)
                            <option value="{{$generation->id}}"> {{$generation->name}} </option>

                            @endforeach
                          </select>
                    </fieldset>







                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">اغلاق</button>
                    <button type="submit" class="btn btn-outline-primary">اضافة</button>
                </form>
                  </div>
                </div>
              </div>
            </div>



        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>

                          <th>دفعة</th>
                          <th> السنة </th>
                          <th> تاريخ </th>
                          <th> Delete </th>

                        </tr>
                      </thead>
                    <tbody>
                        @foreach ($generationYears as $item)
                            <tr>
<td> {{$item->generation->name}} </td>
<td> {{$item->year->name}}  </td>
<td> {{$item->university_calender->year}}  </td>
<td> <a class="btn btn-danger" href="/admin/removegenerationyear/{{$item->id}}">Del</a> </td>
                            </tr>
                        @endforeach

                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>


@endsection
