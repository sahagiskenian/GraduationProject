@extends('layouts.adminlayout')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">
           معرض {{Auth::user()->college->name}}
          </h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>

        <div class="content-header-right  offset-3 col-md-3 col-12">



            <!-- Modal -->




        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>

                          <th>اسم المشروع</th>
                          <th>القسم</th>
                          <th> السنة</th>
                          <th>  تفاصيل </th>
                          <th>  الشعار </th>
                          <th> حذف </th>
                        </tr>
                      </thead>
                    <tbody>
                        @foreach ($galleries as $gallery)
                            <tr>
 <td>{{$gallery->name}}</td>
 <td>{{$gallery->department->name}}</td>
 <td>{{$gallery->university_calender->year}}</td>
 <td><a href="/admin/gallery/{{$gallery->id}}" class="btn btn-primary"  > Details </a></td>
 <td><img class="img-fluid" src="{{asset($gallery->logo)}}" /></td>
<td> <a class="btn btn-danger" href="/admin/removegallery/{{$gallery->id}}">Del</a> </td>
                            </tr>
                        @endforeach

                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>


@endsection
