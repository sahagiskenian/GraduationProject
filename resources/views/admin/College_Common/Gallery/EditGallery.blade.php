@extends('layouts.adminlayout')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">   Gallery </h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>
        <div class="content-header-right  offset-3 col-md-3 col-12">

            <!-- Modal -->




        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <h2>    Edit Gallery </h2>
           <form action="/admin/gallery" method="POST"  enctype="multipart/form-data">
            @csrf
            <input hidden type="text" name="id" value="{{$gallery->id}}" class="form-control" />


            <div class="row">
                <div class="form-group col-md-6">
                    <label > Name
                  <input type="text" name="name" value="{{$gallery->name}}" class="form-control" />
                </label>
                </div>
                <div class="form-group col-md-6">
                    <label >Description
                  <textarea type="text" name="description"   class="form-control" >{{$gallery->description}}</textarea>
                </label>
                </div>
                <div class="form-group col-md-6">
                    <label > Logo
                  <input type="file" name="logo" value="{{$gallery->logo}}" class="form-control" />
                  <img class="img-fluid" src={{asset($gallery->logo)}} />
                </label>
                </div>




            </div>








<div class="row">
    <div class="offset-8"> </div>
    <div class="col-md-4">
    <button type="submit" class="btn btn-primary">Edit</button>
    </div>
</div>
           </form>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header">

                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard  ">

                    <h2>   Gallery Students  </h2>
                    <div class="content-header-right  offset-3 col-md-3 col-12">
                        <button type="button" class="btn btn-outline-primary block btn-lg" data-toggle="modal"
                        data-target="#default">
                         Add Student
                        </button>
                        <!-- Modal -->
                        <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
                        aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel1">Student Gallery  </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                             <form action="/admin/gallerystudent" method="POST"  enctype="multipart/form-data">
                                @csrf


                                    <input hidden name="gallery_id"  value="{{$gallery->id}}"  type="text" class="form-control"    >

                                <fieldset class="form-group">
                                    <label> Users</label>
                                  <select class="form-control" name="user_id" >
                                  @foreach ($users as $user)
                                      <option value="{{$user->id}}">{{$user->first_name}} {{$user->last_name}} </option>
                                  @endforeach

                                  </select>
                                  </fieldset>



                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-outline-primary">Add</button>
                            </form>
                              </div>
                            </div>
                          </div>
                        </div>


                    </div>
            <div class="row">
                <table class="table table-striped table-bordered zero-configuration">
                    <thead>
                      <tr>

                        <th>First Name</th>

                         <th>Last Name</th>

                        <th>Delete</th>
                      </tr>
                    </thead>
                  <tbody>
                      @foreach ($gallery->galleryusers as $user)
                          <tr>

<td> {{$user->user->first_name}} </td>

<td> {{$user->user->last_name}} </td>


<td> <a href="/admin/gallerystudent/{{$gallery->id}}/{{$user->user->id}}" class="btn btn-danger"> Delete </a> </td>
                          </tr>
                      @endforeach

                  </tbody>


                  </table>

            </div>








                </div>
              </div>
            </div>

              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>
<script src="{{asset('app-assets/js/scripts/forms/form-repeater.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
      var i=0;
     $("#category_id").on('change',function(e){
  console.log("ss");
  var value=e.target.value;

         $.ajax({
             url:'/api/category/'+value,
             type:'get',
             dataType:'json',




             success:function(response){
                 console.log(response);
              $("#subcategory_id").empty();
                 if(response.length==0){
                  $("#subcategory_id").empty();
                  var _html= `    <option value="" selected>Open this select menu</option> `
      $("#subcategory_id").append(_html);

                 }
                 else {
  console.log(response);
  $("#subcategory_id").append(`    <option value="" selected>Open this select menu</option> `);
  $.each(response ,function(index,value){

          var _html= `     <option value="${value.id}">${value.subcategory_en}</option> `
      $("#subcategory_id").append(_html);

                   });



                 }
               }


         });
     });








     });






  </script>
@endsection
