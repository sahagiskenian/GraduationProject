@extends('layouts.adminlayout')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">     تابات</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>

        <div class="content-header-right  offset-3 col-md-3 col-12">

            <button type="button" class="btn btn-outline-primary block btn-lg" data-toggle="modal"
            data-target="#default">
                اضافة تاب
            </button>

            <!-- Modal -->
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
            aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1"> تاب </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                 <form action="/admin/AddTab" method="POST" enctype="multipart/form-data">
                    @csrf
                    <fieldset class="form-group">
                        <input name="name" placeholder="   الاسم" type="text" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group">
                        <input name="type" placeholder=" نوع  " type="text" class="form-control" id="basicInput">
                      </fieldset>
                         <fieldset class="form-group">
                      <label>Public</label>
                        <input name="is_public" placeholder=" public  " type="checkbox" value="1" class="form-control" >
                      </fieldset>
                      <fieldset class="form-group">
                      @if (Auth::user()->user_type_id==2)
                          <input name="university_id" value="{{Auth::user()->university_id}}" hidden />
                      @elseif (Auth::user()->user_type_id==3)
                           <label>College</label>
                           <input name="college_id" type="checkbox" value="{{Auth::user()->college_id}}"   />
                           <label>Specialization</label>
                           <input name="specialization_id" type="checkbox" value="{{Auth::user()->college->specialization_id}}"   />
                      @endif


                      </fieldset>







                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">اغلاق</button>
                    <button type="submit" class="btn btn-outline-primary">اضافة</button>
                </form>
                  </div>
                </div>
              </div>
            </div>



        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>

                          <th>Name</th>
                          <th> Public</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                    <tbody>

                        @foreach($tabs as $tab)
                        <tr>
                            <td> {{$tab->name}} </td>
                            <td>
                            @if ($tab->is_public)
                                Yes
                            @else
                                No
                            @endif

                             </td>


                            <td> <a href="/admin/RemoveTab/{{$tab->id}}" class="btn btn-danger"> حذف </a> </td>
                        </tr>
                        @endforeach



                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>
@endsection
