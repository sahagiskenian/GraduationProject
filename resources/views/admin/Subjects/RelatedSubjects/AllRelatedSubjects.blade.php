@extends('layouts.adminlayout')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">
              مواد المشتركة من كليتي مع باقي الكليات
          </h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>

        <div class="content-header-right  offset-3 col-md-3 col-12">

            <button type="button" class="btn btn-outline-primary block btn-lg" data-toggle="modal"
            data-target="#default">
            ربط مواد
            </button>

            <!-- Modal -->
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
            aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">
                    مواد
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                 <form action="/admin/relatedsubjects" method="POST">
                    @csrf
                    <fieldset class="form-group">
                        <select class="form-control" name="first_subject_id" aria-label="Default select example">
                            <option selected>مادة كليتي </option>
                            @foreach ($mainsubjects as $subject)
                            <option value="{{$subject->id}}"> {{$subject->name}} </option>

                            @endforeach
                          </select>
                           </fieldset>

                      <fieldset class="form-group">

                        <select id="university_id" class="form-control" name="university_id"  aria-label="Default select example">
                            <option selected>     جامعة</option>
                            @foreach ($universities as $university)
                            <option value="{{$university->id}}"> {{$university->name}} </option>

                            @endforeach
                          </select>
                    </fieldset>
                    <fieldset class="form-group">

                        <select id="college_id" class="form-control" name="college_id"  aria-label="Default select example">
                            <option selected>مادة كلية اخرى</option>

                          </select>
                    </fieldset>
                    <fieldset class="form-group">

                        <select class="form-control" name="department_id" id="department_id" aria-label="Default select example">
                            <option selected>  اقسام  كلية  </option>

                          </select>
                    </fieldset>


                    <fieldset class="form-group">

                        <select class="form-control" name="subject_id" id="subject_id" aria-label="Default select example">
                            <option selected>  مواد  كلية  </option>

                          </select>
                    </fieldset>




                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">اغلاق</button>
                    <button type="submit" class="btn btn-outline-primary">اضافة</button>
                </form>
                  </div>
                </div>
              </div>
            </div>



        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>

                          <th>مادتي</th>
                          <th>مادة كلية اخرى</th>
                          <th> Delete </th>

                        </tr>
                      </thead>
                    <tbody>
                        @foreach ($related as $relate)
                            <tr>
<td> {{$relate->FirstSubject->name}} </td>
<td> {{$relate->SecondSubject->department->college->university->name}}-
    {{$relate->SecondSubject->department->college->name}}-
    {{$relate->SecondSubject->department->name}}-
    {{$relate->SecondSubject->name}}  </td>
<td> <a class="btn btn-danger" href="/admin/removerelatedsubjects/{{$relate->id}}">Del</a> </td>
                            </tr>
                        @endforeach

                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
      var i=0;
     $("#university_id").on('change',function(e){
  console.log("ss");
  var value=e.target.value;

         $.ajax({
             url:'/api/colleges/'+value,
             type:'get',
             dataType:'json',




             success:function(response){
                 console.log(response);
              $("#college_id").empty();
                 if(response.data.length==0){
                  $("#college_id").empty();
                  var _html= `    <option selected>Open this select menu</option> `
      $("#college_id").append(_html);

                 }
                 else {
  console.log(response);
  $("#college_id").append(`    <option selected>Open this select menu</option> `);
  $.each(response.data ,function(index,value){

          var _html= `     <option value="${value.id}">${value.name}</option> `
      $("#college_id").append(_html);

                   });



                 }
               }


         });
     });





     $("#college_id").on('change',function(e){

  var value=e.target.value;

         $.ajax({
             url:'/api/department/'+value,
             type:'get',
             dataType:'json',




             success:function(response){
                 console.log(response);
              $("#department_id").empty();
                 if(response.data.length==0){
                  $("#department_id").empty();
                  var _html= `    <option selected>Open this select menu</option> `
      $("#department_id").append(_html);

                 }
                 else {
  console.log(response);
  $("#department_id").append(`    <option selected>Open this select menu</option> `);
  $.each(response.data ,function(index,value){

          var _html= `     <option value="${value.id}">${value.name}</option> `
      $("#department_id").append(_html);

                   });



                 }
               }


         });
     });


     $("#department_id").on('change',function(e){

  var value=e.target.value;

         $.ajax({
             url:'/api/subject/'+value,
             type:'get',
             dataType:'json',




             success:function(response){
                 console.log(response);
              $("#subject_id").empty();
                 if(response.data.length==0){
                  $("#subject_id").empty();
                  var _html= `    <option selected>Open this select menu</option> `
      $("#subject_id").append(_html);

                 }
                 else {
  console.log(response);
  $("#subject_id").append(`    <option selected>Open this select menu</option> `);

  $.each(response.data ,function(index,value){

          var _html= `     <option value="${value.id}">${value.name}</option> `
      $("#subject_id").append(_html);

                   });



                 }
               }


         });
     });



    });


  </script>

@endsection
