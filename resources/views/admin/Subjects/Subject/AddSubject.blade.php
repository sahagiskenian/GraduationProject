@extends('layouts.adminlayout')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">   المواد </h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>
        <div class="content-header-right  offset-3 col-md-3 col-12">

            <!-- Modal -->




        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <h2> اضافة مادة </h2>
           <form action="/admin/subjects" method="POST">
            @csrf
<div class="form-group">
    <select class="form-control" name="department_id" aria-label="Default select example">
        <option selected>القسم</option>
      @foreach ($departments as $department)
      <option value="{{$department->id}}"> {{$department->name}} </option>

      @endforeach
      </select>

</div>
<div class="form-group">
    <select class="form-control" name="semester_id" aria-label="Default select example">
        <option selected>الفصل</option>
        @foreach ($semesters as $semester)
        <option value="{{$semester->id}}"> {{$semester->semester}} </option>

        @endforeach
      </select>

</div>
<div class="form-group">
    <select class="form-control" name="year_id" aria-label="Default select example">
        <option selected>السنة</option>
        @foreach ($years as $year)
        <option value="{{$year->id}}"> {{$year->name}} </option>

        @endforeach
      </select>

</div>
<div class="form-group">
    <input type="text" name="name" class="form-control" placeholder="اسم المادة" />

</div>
<div class="form-group">
    <textarea  name="description"  class="form-control" placeholder="شرح المادة"  ></textarea>

</div>
<div class="row">
    <div class="offset-8"> </div>
    <div class="col-md-4">
    <button type="submit" class="btn btn-primary">اضافة</button>
    </div>
</div>
           </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>
@endsection
