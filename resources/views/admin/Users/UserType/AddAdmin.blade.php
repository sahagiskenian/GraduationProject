@extends('layouts.adminlayout')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">  @if ($user_id==1)

                         @elseif($user_id==2)
                         مدراء الجامعات
                         @elseif($user_id==3)
                         مدراء الكليات
                         @elseif($user_id==4)
                      المدراء
                     @endif</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>

        <div class="content-header-right  offset-3 col-md-3 col-12">
            @if($user_id != 5 && $user_id !=6)
            <button type="button" class="btn btn-outline-primary block btn-lg" data-toggle="modal"
            data-target="#default">
             اضافة ادمن
            </button>
            @endif
            <!-- Modal -->
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
            aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">@if ($user_id==1)
           مدراء الجامعات
                        @elseif($user_id==2)
مدراء الكليات
                        @elseif($user_id==3)
مدراء الدفعات
                        @elseif($user_id==4)
                        الطلاب
                    @else

                    @endif </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                 <form action="/admin/createAdmin" method="POST">
                    @csrf
                    <fieldset class="form-group">
                        <input name="first_name" placeholder="   الاسم" type="text" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group">
                        <input name="last_name" placeholder="الكنية " type="text" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group">
                        <input name="email" placeholder="الايميل" type="email" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group">
                        <input name="password" placeholder="كلمة السر" type="password" class="form-control" id="basicInput">
                      </fieldset>

                      <fieldset class="form-group">
                        <input name="mobile" placeholder=" رقم الموبايل  " type="number" class="form-control" id="basicInput">
                      </fieldset>
                      @if (Auth::user()->user_type_id==1)
                      <fieldset class="form-group">
                        <select class="form-select" name="university_id" aria-label="Default select example">
                          <option selected>تحديد الجامعة</option>
                          @foreach ($university as $uni)
                              <option value="{{$uni->id}}"> {{$uni->name}} </option>
                          @endforeach
                        </select>
                        </fieldset>
                      @endif

                      @if (Auth::user()->user_type_id==2)
                      <fieldset class="form-group">
                        <input name="university_id" value="{{Auth::user()->university_id}}"  hidden type="number" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group">
                        <select class="form-select" name="college_id" aria-label="Default select example">
                          <option selected>تحديد الكلية</option>
                          @foreach ($colleges as $college)
                          @if ($college->university_id==Auth::user()->university->id)
                          <option value="{{$college->id}}"> {{$college->name}} </option>
                          @endif

                          @endforeach
                        </select>
                        </fieldset>
                      @endif
                      <fieldset class="form-group">
                        <input name="user_type_id" value="{{Auth::user()->user_type_id + 1}}"  hidden type="number" class="form-control" id="basicInput">
                      </fieldset>



                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">اغلاق</button>
                    <button type="submit" class="btn btn-outline-primary">اضافة</button>
                </form>
                  </div>
                </div>
              </div>
            </div>



        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                            <th>Email</th>
                          <th>Name</th>
                          @if (Auth::user()->user_type_id==1)
                              <th> University </th>
                          @elseif(Auth::user()->user_type_id==2)
                          <th> College </th>
                          @endif

                          @if ($user_id==5)
                      <th>   Accept     </th>
                          @endif

                          <th>Delete</th>
                        </tr>
                      </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
<td> {{$user->email}} </td>
<td> {{$user->first_name}} </td>
@if (Auth::user()->user_type_id==1)
<td> {{$user->university->name}} </td>
@elseif(Auth::user()->user_type_id==2)
<td> {{$user->college->name}} </td>
@endif
@if ($user_id==5)
                      <td><a href="/admin/AcceptStudent/{{$user->id}}" class="btn btn-primary"> قبول    </a>    </td>
                          @endif

<td> <a href="/admin/RemoveAdmin/{{$user->id}}" class="btn btn-danger"> حذف  </a> </td>
                            </tr>
                        @endforeach

                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>
@endsection
