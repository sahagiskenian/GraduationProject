<link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <link rel="stylesheet" href="{{asset("webassets/css/bootstrap.css")}}" type="text/css" />
   <link rel="stylesheet" href="{{asset("webassets/css/bootstrap-rtl.css")}}" type="text/css" />
    <link rel="stylesheet" href="{{asset("webassets/style.css")}}" type="text/css" />
    <link rel="stylesheet" href="{{asset("webassets/style-rtl.css")}}" type="text/css" />
	<link rel="stylesheet" href="{{asset("webassets/css/swiper.css")}}" type="text/css" />
	<link rel="stylesheet" href="{{asset("webassets/css/dark.css")}}" type="text/css" />
	<link rel="stylesheet" href="{{asset("webassets/css/font-icons.css")}}" type="text/css" />
	<link rel="stylesheet" href="{{asset("webassets/css/animate.css")}}" type="text/css" />
	<link rel="stylesheet" href="{{asset("webassets/css/magnific-popup.css")}}" type="text/css" />

    <link rel="stylesheet" href="{{asset("webassets/css/custom.css")}}" type="text/css" />

    <script type="application/javascript" src="{{ asset('js/app.js') }}" defer></script>

