<!DOCTYPE html>
<html dir="rtl" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	 @include('layouts.styles.webstyles')
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Home - Blog Layout | Canvas</title>

</head>

<body class="stretched rtl">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">



        @yield('content')
	@include("layouts.weblayouts.footer")

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

@include("layouts.scripts.webscripts")

</body>
</html>
