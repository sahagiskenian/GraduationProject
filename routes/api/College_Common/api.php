<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\College_Common\CollegeController;
use App\Http\Controllers\API\College_Common\CommonQuestionController;
use App\Http\Controllers\API\College_Common\DepartmentController;
use App\Http\Controllers\API\College_Common\GalleryController;
use App\Http\Controllers\API\University_Common\UniversityController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//University Colleges
Route::get('/colleges/{id}', [CollegeController::class, 'GetUniversityColleges']);

//College Departments
Route::get('/department/{id}', [CollegeController::class, 'GetCollegeDepartments']);


Route::get('/commonquestions', [CommonQuestionController::class, 'GetAllCommonQuestions']);

Route::get('/commonquestions/{id}', [CommonQuestionController::class, 'GetCollegeCommonQuestions']);

Route::get('/collegedetails/{id}', [CollegeController::class, 'GetCollegeDetails']);

Route::get('/collegegallery/{id}', [GalleryController::class, 'GetCollegeGallery']);
