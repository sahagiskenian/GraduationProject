<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\College_Common\CollegeController;
use App\Http\Controllers\API\College_Common\DepartmentController;
use App\Http\Controllers\API\Feedback\AdvertisingController;
use App\Http\Controllers\API\Feedback\FeedbackController;
use App\Http\Controllers\API\University_Common\UniversityController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//post Feedback
Route::post('/feedback', [FeedbackController::class, 'AddFeedback']);

Route::get('/advertisings', [AdvertisingController::class, 'get']);



