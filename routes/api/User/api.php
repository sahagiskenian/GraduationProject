<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Auth\AuthController;
use App\Http\Controllers\API\User\CategorySaveController;
use App\Http\Controllers\API\User\SaveController;
use App\Http\Controllers\API\User\UserSearchController;
use App\Models\User\CategorySave;
use Illuminate\Support\Facades\Hash;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['cors']], function () {
    Route::post('/login', [AuthController::class, 'login'])->name('login.api');
    Route::post('/register',[AuthController::class, 'register'])->name('register.api');

});

Route::middleware('auth:api')->group(function () {
    // our routes to be protected will go in here
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout.api');



    Route::get('/searchs', [UserSearchController::class, 'get'])->name('search.api');

    Route::post('/postsearch', [UserSearchController::class, 'post'])->name('postSearch.api');

    Route::post('/clearsearchs', [UserSearchController::class, 'remove'])->name('searchclear.api');


    Route::get('/categorysave', [CategorySaveController::class, 'get'])->name('categorysave.api');

    Route::post('/categorysave', [CategorySaveController::class, 'post'])->name('postcategorysave.api');

    Route::get('/removecategorysave/{id}', [CategorySaveController::class, 'delete'])->name('removecategorysave.api');


    Route::post('/save', [SaveController::class, 'post'])->name('postsave.api');

    Route::get('/removesave/{id}', [SaveController::class, 'delete'])->name('removesave.api');
});
