<?php

use App\Http\Controllers\API\Social\CommentController;
use App\Http\Controllers\API\Social\ConversationController;
use App\Http\Controllers\API\Social\ConversationGroupUserController;
use App\Http\Controllers\API\Social\ConversationMessageController;
use App\Http\Controllers\API\Social\PostController;
use App\Http\Controllers\API\Social\RateController;
use App\Http\Controllers\API\Social\ReactController;
use App\Http\Controllers\API\Social\TabController;
use App\Models\Social\ConversationMessage;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->group(function(){

    Route::prefix('post')->group(function () {

        Route::post('create',[PostController::class,'create']);
        Route::put('update',[PostController::class,'update']);
        Route::delete('delete',[PostController::class,'delete']);
        Route::get('find',[PostController::class,'find']);
        Route::get('get',[PostController::class,'getTabPosts']);
        Route::get('search',[PostController::class,'search']);

        Route::prefix('general')->group( function()
        {
            Route::get('get',[PostController::class,'getGeneralQuestion']);
        });


    });

    Route::prefix('comment')->group(function () {
        Route::post('create',[CommentController::class,'create']);
        Route::put('update',[CommentController::class,'update']);
        Route::delete('delete',[CommentController::class,'delete']);
        Route::get('replies',[CommentController::class,'getReplies']);
    });

    Route::prefix('react')->group(function () {
        Route::post('/',[ReactController::class,'react']);
    });

    Route::prefix('rate')->group(function () {
        Route::post('/',[RateController::class,'rate']);
    });

    Route::prefix('tab')->group(function () {
        Route::get('get',[TabController::class,'get']);
    });

    Route::prefix('media')->group(function(){
        Route::post('upload',[PostController::class,'uploadMedia']);
    });

    Route::prefix('conversation')->group(function(){
        Route::get('/',[ConversationController::class,'get']);
        Route::post('/createGroup',[ConversationController::class,'createGroup']);
        Route::post('/addUsers',[ConversationGroupUserController::class,'addUser']);
        Route::get('/getUsers',[ConversationGroupUserController::class,'getUsers']);


    });

    Route::prefix('conversationMessage')->group(function(){
        Route::post('/create',[ConversationMessageController::class,'create']);
        Route::get('/get',[ConversationMessageController::class,'get']);

    });


    Route::prefix('subjects')->group(function()
    {
        Route::get('/get_filters',[PostController::class,'generalQuestionFilters']);
        Route::get('/year_semester',[PostController::class,'getUserYearSemesterSubject']);
    });



});
