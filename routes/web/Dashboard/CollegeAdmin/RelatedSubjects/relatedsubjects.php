<?php

use App\Http\Controllers\Admin\Subjects\SubjectController;
use Illuminate\Support\Facades\Route;


Route::prefix('admin')->middleware(['auth','admin'])->group(function () {

//Get Related Subjects
Route::get('/relatedsubjects', [SubjectController::class, 'RelatedSubjects'])->name('GetRelatedSubjects');

// Post Related Subjects
Route::post('/relatedsubjects', [SubjectController::class, 'RelatedSubjects'])->name('PostRelatedSubjects');

Route::get('/removerelatedsubjects/{id}', [SubjectController::class, 'RemoveRelatedSubjects'])->name('RemoveRelatedSubjects');


});
