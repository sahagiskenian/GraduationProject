<?php

use App\Http\Controllers\Admin\College_Common\GenerationController;
use App\Http\Controllers\Admin\College_Common\GenerationYearController;
use App\Http\Controllers\Admin\Subjects\SubjectController;
use App\Http\Controllers\API\College_Common\GalleryController;
use Illuminate\Support\Facades\Route;


Route::prefix('admin')->middleware(['auth'])->group(function () {

//Get Gallery
Route::get('/gallery', [GalleryController::class, 'GetMyGallery'])->name('GetMyGallery');
//Remove From Gallery
Route::get('/removegallery/{id}', [GalleryController::class, 'RemoveFromMyGallery'])->name('RemoveFromMyGallery');

Route::get('/gallery/{id}', [GalleryController::class, 'GetGalleryDetails'])->name('GetGalleryDetails');

Route::post('/gallerystudent', [GalleryController::class, 'AddStudentToGallery'])->name('AddStudentToGallery');

Route::post('/gallery', [GalleryController::class, 'EditGallery'])->name('EditGallery');

Route::get('/gallerystudent/{gallery_id}/{user_id}', [GalleryController::class, 'RemoveStudentFromGallery'])->name('RemoveStudentFromGallery');
});
