<?php

use App\Http\Controllers\Admin\College_Common\GenerationController;
use App\Http\Controllers\Admin\College_Common\GenerationYearController;
use App\Http\Controllers\Admin\Subjects\SubjectController;

use Illuminate\Support\Facades\Route;


Route::prefix('admin')->middleware(['auth'])->group(function () {

//Get Generations
Route::get('/generations', [GenerationController::class, 'GetGenerations'])->name('GetGetGeneration');

// Post Generation
Route::post('/addgeneration', [GenerationController::class, 'AddGeneration'])->name('PostAddGeneration');
//Remove Generation
Route::get('/removegeneration/{id}', [GenerationController::class, 'RemoveGeneration'])->name('RemoveGeneration');


//Get Generations
Route::get('/generationyears', [GenerationYearController::class, 'GetGenerationYear'])->name('generationyears');

// Post Generation
Route::post('/addgenerationyear', [GenerationYearController::class, 'AddGenerationYear'])->name('addgenerationyears');
//Remove Generation
Route::get('/removegenerationyear/{id}', [GenerationYearController::class, 'RemoveGenerationYear'])->name('removegenerationyears');

});
