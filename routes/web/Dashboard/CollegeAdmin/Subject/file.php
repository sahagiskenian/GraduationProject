<?php

use App\Http\Controllers\Admin\Subjects\FileController;
use Illuminate\Support\Facades\Route;


Route::prefix('admin')->middleware(['auth','admin'])->group(function () {

//All files
Route::get('/files', [FileController::class, 'GetFiles']);



//Post file
Route::post('/AddFile', [FileController::class, 'AddFile']);

//delete file
Route::get('/removefile/{id}', [FileController::class, 'RemoveFile'])->name('RemoveFile');

});
