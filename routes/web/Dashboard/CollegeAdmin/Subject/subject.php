<?php

use App\Http\Controllers\Admin\Subjects\SubjectController;
use Illuminate\Support\Facades\Route;


Route::prefix('admin')->middleware(['auth','admin'])->group(function () {

//All My Subjects
Route::get('/allsubjects', [SubjectController::class, 'GetSubjects'])->name('GetAllSubjects');

//Get Post Subject Page
Route::get('/subjects', [SubjectController::class, 'AddSubject'])->name('AddSubjectGet');

//Post Subject
Route::post('/subjects', [SubjectController::class, 'AddSubject'])->name('AddSubjectPost');

//delete subject
Route::get('/subject/{id}', [SubjectController::class, 'RemoveSubject'])->name('RemoveSubject');

});
