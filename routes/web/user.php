<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\Profile\ProfileController;




Route::get('/profile', [ProfileController::class,"index"]);




Route::get('/{any?}', function () {

    return view('web.main.home');
})->where('any', '^(?!api\/)[\/\w\.-]*');
