<?php

use App\Http\Controllers\Admin\Feedback\FeedbackController;
use App\Http\Controllers\Admin\Social\TabsController;
use App\Http\Controllers\Admin\Subjects\SubjectController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\Users\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;



Route::prefix('admin')->middleware(['auth','admin'])->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
//returns user types
    Route::get('/userTypes', [UserController::class, 'getUserTypes'])->name('getUserTypes');
//returns add Admin page
    Route::get('/addAdmin/{id}', [UserController::class, 'addAdmin'])->name('addAdmin');
//posts an admin
    Route::post('/createAdmin', [UserController::class, 'createAdmin'])->name('createAdmin');
//remove the admin
Route::get('/RemoveAdmin/{id}', [UserController::class, 'RemoveAdmin'])->name('RemoveAdmin');

//Accept Student
Route::get('/AcceptStudent/{id}', [UserController::class, 'AcceptStudent'])->name('AcceptStudent');
Route::get('/RemoveTab/{id}', [TabsController::class, 'RemoveTab'])->name('RemoveTab');

Route::post('/AddTab', [TabsController::class, 'AddTab'])->name('AddTab');


Route::get('/Feedbacks', [FeedbackController::class, 'GetFeedbacks'])->name('GetFeedbacks');

Route::get('/removefeedback/{id}', [FeedbackController::class, 'RemoveFeedback'])->name('RemoveFeedback');
});

Route::get('/Tabs', [TabsController::class, 'GetTabs'])->name('GetTabs');



