<?php

use App\Http\Controllers\Admin\Subjects\SubjectController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\Users\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Auth;

Route::get('/login', [LoginController::class, 'getLogin'])->name('getLogin');
Route::post('/login', [LoginController::class, 'authenticate'])->name('login');
Route::get('/register', [RegisterController::class, 'GetRegister'])->name('GetRegister');
Route::post('/register', [RegisterController::class, 'AddStudent'])->name('register');
Auth::routes(['login'=>false,'register'=>false]);
