<?php


namespace App\Repositories\Implementations\Subject;

use App\Models\Subject\UserSubjects;
use App\Repositories\Interfaces\Subject\UserSubjectsInterface;

class UserSubjectsRepository implements UserSubjectsInterface {



    public function create($data)
    {
        return UserSubjects::create($data);
    }

    public function checkUserSubject($userId, $subjectId)
    {
        return UserSubjects::where('user_id',$userId)
                             ->where('subject_id',$subjectId)
                             ->first();
    }

}
