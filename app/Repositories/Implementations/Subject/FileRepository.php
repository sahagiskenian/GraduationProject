<?php

namespace App\Repositories\Implementations\Subject;

use App\Repositories\Interfaces\Subject\SubjectInterface;


use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use App\Models\College_Common\College;
use App\Models\College_Common\GenerationYear;
use App\Models\Subject\CommonSubject;
use App\Models\Subject\File;
use App\Models\Subject\Subject;
use App\Models\University_Common\University;
use App\Repositories\Interfaces\Subject\FileInterface;
use Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use RealRashid\SweetAlert\Facades\Alert as FacadesAlert;

class FileRepository implements FileInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }


   public function GetFiles()
   {
    $subjects=College::with(['departments' => function ($query) {
        $query->with(['subjects' => function ($query) {
            $query->with('files')->get();
        }]);
    }])->where('id',FacadesAuth::user()->college_id)->first();
    $generationYears=GenerationYear::with(['generation' => function ($query) {
        $query->where('college_id',FacadesAuth::user()->college_id)->get();
    }])->with(['year' => function ($query) {
        $query->where('college_id',FacadesAuth::user()->college_id)->get();
    }])->get();


    return view('admin.Subjects.Files.Files')
          ->with('generationYears',$generationYears)
          ->with('subjects',$subjects);

   }


public function AddFile($array)
{
    $file=new File();

    $file->name=$array->name;
    $file->subject_id=$array->subject_id;
    $file->generation_year_id=$array->generation_year_id;

    $mainImage = $array->file('url');
    if($mainImage) {

      $filename    = time().".".$mainImage->getClientOriginalExtension();

      $destinationPath = public_path('uploads/files/');
      $mainImage->move($destinationPath, $filename);

          $file->url='uploads/files/'.$filename;
    }
    $file->save();

    FacadesAlert::success("Added Succesfully");


    return redirect()->back();
}


public function RemoveFile($id){


    File::find($id)->delete();


    FacadesAlert::success("deleted Successfully");


    return redirect()->back();

}




}
