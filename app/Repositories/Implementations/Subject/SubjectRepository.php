<?php

namespace App\Repositories\Implementations\Subject;

use App\Repositories\Interfaces\Subject\SubjectInterface;


use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use App\Models\College_Common\College;
use App\Models\Subject\CommonSubject;
use App\Models\Subject\Subject;
use App\Models\Subject\UserSubjects;
use App\Models\University_Common\University;
use App\Models\User\User;
use Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use RealRashid\SweetAlert\Facades\Alert as FacadesAlert;

class SubjectRepository implements SubjectInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {
    }


    public function addSubject($array)
    {
        $newSubject =  Subject::create([
            "name" => $array->name,
            "description" => $array->description,
            "year_id" => $array->year_id,
            "semester_id" => $array->semester_id,
            "department_id" => $array->department_id
        ]);

        FacadesAlert::success(" تم اضافة المادة بنجاح ");


        return redirect()->back();
    }

    public function RemoveSubject($id)
    {
        Subject::find($id)->delete();

        FacadesAlert::success(" تم حذف المادة بنجاح ");


        return redirect()->back();
    }

    public function GetSubjects()
    {
        $subjects = College::with(['departments' => function ($query) {
            $query->with(['subjects' => function ($query) {
                $query;
            }]);
        }])->where('id', FacadesAuth::user()->college_id)->first();




        return $subjects;
    }

    public function AddRelatedSubjects($array)
    {
        $Related = CommonSubject::create([
            "first_subject_id" => $array->first_subject_id,
            "second_subject_id" => $array->subject_id,
        ]);

        FacadesAlert::success(" تم ربط المواد بنجاح ");

        return redirect()->back();
    }
    public function GetRelatedSubjects()
    {
        $RelatedSubjects = CommonSubject::with('FirstSubject')->with(['SecondSubject' => function ($query) {
            $query->with(['department' => function ($query) {
                $query->with(['college' => function ($query) {
                    $query->with(['university' => function ($query) {
                        $query;
                    }]);
                }]);
            }]);
        }])->get();
        $Universities = University::all();
        $mainsubjects = Subject::with(['department' => function ($query) {
            $query->with(['college' => function ($query) {
                $query->where('id', FacadesAuth::user()->college_id);
            }]);
        }])->get();

        return view('admin.Subjects.RelatedSubjects.AllRelatedSubjects')
            ->with('related', $RelatedSubjects)
            ->with('universities', $Universities)
            ->with('mainsubjects', $mainsubjects);
    }
    public function RemoveRelatedSubjects($id)
    {
        $relation = CommonSubject::find($id)->delete();

        FacadesAlert::success(" تم ازالة ربط المواد بنجاح ");

        return redirect()->back();
    }

    public function checkUserSubject(User $user,$subjectId)
    {
       return Subject::where('id',$subjectId)
       ->whereHas('userSubject',function($userSubject) use ($user)
        {
            $userSubject->where('user_id',$user->id);
        })->orWhereHas('secondCommonSubject',function($secondCommonSubject) use ($user)
        {
            $secondCommonSubject->join('user_subjects','user_subjects.subject_id','=','common_subjects.first_subject_id')
            ->where('user_subjects.user_id',$user->id);
        })
        ->first();

    }
}
