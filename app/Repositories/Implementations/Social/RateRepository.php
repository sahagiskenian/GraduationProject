<?php

namespace App\Repositories\Implementations\Social;

use App\Models\Social\Rating;
use App\Repositories\Interfaces\Social\RateInterface;

class RateRepository implements RateInterface {


    public function create($data)
    {
        return Rating::create($data);
    }

    public function find($filter)
    {
        return Rating::where($filter)
        ->first();
    }

    public function updateOrCreate($filter,$data)
    {
        return Rating::updateOrCreate($filter,$data);
    }

    public function delete($filter)
    {
        return Rating::where($filter)
                        ->forceDelete();
    }
}
