<?php

namespace App\Repositories\Implementations\Social;




use Illuminate\Database\Eloquent\Model;
use App\Models\Social\Tabs;
use App\Models\User\User;
use App\Repositories\Interfaces\Social\TabsInterface;
use RealRashid\SweetAlert\Facades\Alert as FacadesAlert;

class TabsRepository implements TabsInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {
    }


    public function AddTab($array)
    {
        $tab = new Tabs();
        $tab->name = $array->name;
        $tab->type = $array->type;

        if ($array->has('is_public')) {
            $tab->is_public = $array->is_public;
        }
        if ($array->has('university_id')) {
            $tab->university_id = $array->university_id;
        }
        if ($array->has('college_id')) {
            $tab->college_id = $array->college_id;
        }
        if ($array->has('specialization_id')) {
            $tab->specialization_id = $array->specialization_id;
        }
        if ($array->has('generation_year_id')) {
            $tab->generation_year_id = $array->generation_year_id;
        }
        $tab->save();
        FacadesAlert::success("Tab Added Successfully");


        return redirect()->back();
    }
    public function GetTabs($university_id = null, $college_id = null, $specialization_id = null, $generation_year_id = null)
    {
        $tabs = Tabs::select();

        if ($university_id) {
            $tabs = $tabs->where('university_id', $university_id)->get();
        }
        if ($college_id) {
            $tabs = $tabs->where('college_id', $college_id)->orWhere('specialization_id', $specialization_id)->get();
        }

        if ($generation_year_id) {
            $tabs = $tabs->orWhere('generation_year_id', $generation_year_id)->get();
        }




        return view('admin.Social.Tabs.Tab')
            ->with('tabs', $tabs);
    }

    public function RemoveTab($id)
    {
        Tabs::find($id)->delete();



        FacadesAlert::success("Tab Removed Succesffuly ");


        return redirect()->back();
    }


    public function GetUserTabs(User $user)
    {

        $filter = $this->__getTabUserForeignKeys($user);

        return Tabs::where('university_id', $filter['university_id'])
            ->orWhere('college_id', $filter['college_id'])
            ->orWhere('generation_year_id', $filter['generation_year_id'])
            ->orWhere('specialization_id', $filter['specialization_id'])
            ->get();
    }

    public function checkUserTab(User $user, $tabId)
    {
        $filter = $this->__getTabUserForeignKeys($user);

        return Tabs::where('id', $tabId)
            ->where(function ($tab) use ($filter) {
                $tab->where('university_id', $filter['university_id'])
                    ->orWhere('college_id', $filter['college_id'])
                    ->orWhere('generation_year_id', $filter['generation_year_id'])
                    ->orWhere('specialization_id', $filter['specialization_id']);
            })->first();
    }

    public function __getTabUserForeignKeys(User $user)
    {
        $data['university_id'] = (empty($user->university_id)) ? optional($user->college)->university->id : $user->university_id;
        $data['university_id'] = empty($data['university_id']) ? 0 : $data['university_id'];

        $data['college_id'] = (empty($user->college_id)) ? 0 : $user->college_id;

        $data['specialization_id'] =  empty($user->college) ? 0 : $user->college->specialization_id;

        $data['generation_year_id'] = (empty($user->college)) ? 0 : optional($user->currentGenrationUserYear)->generation_year_id;
        $data['generation_year_id'] = empty($data['generation_year_id']) ? 0 : $data['generation_year_id'];

        return $data;
    }
}
