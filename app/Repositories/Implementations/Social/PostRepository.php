<?php

namespace App\Repositories\Implementations\Social;

use App\Models\College_Common\Year;
use App\Models\Social\Post;
use App\Models\Subject\Subject;
use App\Models\University_Common\Semester;
use App\Models\User\User;
use App\Repositories\Interfaces\Social\PostInterface;

class PostRepository implements PostInterface
{

    public function create($data)
    {
        return Post::create($data);
    }

    public function update($id, $data)
    {
        return Post::where('id', $id)
            ->update($data);
    }

    public function delete($id)
    {
        return Post::where('id', $id)
            ->delete();
    }

    public function search($text, $tab_id)
    {

        return Post::where('tab_id', $tab_id)
            ->where('title', 'like', $text . '%')
            ->orWhere('title', 'like', '%' . $text . '%')
            ->orWhere('title', 'like', '%' . $text)
            ->orWhere('description', 'like', $text . '%')
            ->orWhere('description', 'like', '%' . $text . '%')
            ->orWhere('description', 'like', '%' . $text)
            ->get();
    }


    public function find($filter)
    {
        return Post::where($filter)
            ->with([
                'comments' => function ($comment) {
                    $comment->with(['reacts.user', 'user', 'comments.user'])
                        ->withSum('rates', 'rate');
                }, 'reacts.user'
            ])
            ->withSum('rates', 'rate')
            ->first();
    }

    public function get($filter)
    {
        return Post::where($filter)
            ->with([
                'comments' => function ($comment) {
                    $comment->with(['reacts.user', 'user', 'comments.user'])
                        ->withSum('rates', 'rate');
                }, 'reacts.user'
            ])
            ->withSum('rates', 'rate')
            ->get();
    }


    public function paginate($filter)
    {
        return Post::where($filter)
            ->with([
                'comments' => function ($comment) {
                    $comment->with(['reacts.user', 'user', 'comments.user'])
                        ->withSum('rates', 'rate');
                }, 'reacts.user'
            ])
            ->withSum('rates', 'rate')
            ->paginate();
    }

    public function getGeneralQuestions(User $user)
    {
      $colleageQuestions = Post::generalQuestion()
            ->whereHas('subject', function ($subject) use ($user) {
                $subject
                    ->join('user_subjects', 'user_subjects.subject_id', '=', 'subjects.id')
                    ->where('user_subjects.user_id', $user->id);
            })->with('subject')
            ->with([
                'comments' => function ($comment) {
                    $comment->with(['reacts.user', 'user', 'comments.user'])
                        ->withSum('rates', 'rate');
                }, 'reacts.user'
            ])
            ->withSum('rates', 'rate')
            ->orderBy('id', 'desc')
            ->get();

        $othersQuestionData = Post::generalQuestion()
            ->join('common_subjects', 'common_subjects.second_subject_id', '=', 'posts.subject_id')
            ->join('user_subjects', 'user_subjects.subject_id', '=', 'common_subjects.first_subject_id')
            ->where('user_subjects.user_id', $user->id)
            ->select(
                'posts.*',
                'user_subjects.user_id',
                'user_subjects.subject_id',
                'common_subjects.first_subject_id',
                'common_subjects.second_subject_id'
            )
            ->with([
                'comments' => function ($comment) {
                    $comment->with(['reacts.user', 'user', 'comments.user'])
                        ->withSum('rates', 'rate');
                }, 'reacts.user'
            ])
            ->withSum('rates', 'rate')
            ->get();

        $subjectsIds = $othersQuestionData->pluck('first_subject_id');

        $collageSubjects = Subject::whereIn('id', $subjectsIds)
            ->get();

        foreach ($othersQuestionData as $otherQuestion) {
            $colleageQuestions->push([
                'id' => $otherQuestion->id,
                'title' => $otherQuestion->title,
                'description' => $otherQuestion->description,
                "created_at" => $otherQuestion->created_at,
                "updated_at" => $otherQuestion->updated_at,
                "deleted_at" => $otherQuestion->deleted_at,
                'subject_id' => $otherQuestion->subject_id,
                'tab_id' => $otherQuestion->tab_id,
                'user_id' => $otherQuestion->user_id,
                'generation_user_year_id' => $otherQuestion->generation_user_year_id,
                'rates_sum_rate' => $otherQuestion->rates_sum_rate,
                'subject' => $collageSubjects->firstWhere('id', $otherQuestion->first_subject_id),
                'comments' => $otherQuestion->comments,
                'reacts' => $otherQuestion->reacts,

            ]);
        }

        $colleageQuestions = $colleageQuestions->sortByDesc(function ($post) {
            return $post['id'];
        })->values();

        return $colleageQuestions;
    }


    public function getGeneralQuestionFilters(User $user)
    {
        $years = Year::where('college_id',$user->college_id)
        ->whereHas('subjects',function($subject) use($user)
        {
                $subject->join('user_subjects','user_subjects.subject_id','=','subjects.id')
                        ->where('user_subjects.user_id',$user->id);
        })->orderBy('id','ASC')
        ->get();

        $semesters = Semester::where('university_id',$user->university_id)
                                ->orderBy('id','ASC')
                                ->get();

       return [
           'semesters' => $semesters,
           'years' => $years,
       ];

    }

    public function getUserYearSemesterSubject(User $user ,$yearId ,$semesterId)
    {
        return Subject::where('year_id',$yearId)
                        ->where('semester_id',$semesterId)
                        ->whereHas('userSubjects',function($userSubjects) use ($user)
                        {
                                $userSubjects->where('user_id',$user->id);
                        })->get();
    }
}
