<?php

namespace App\Repositories\Implementations\Social;

use App\Enums\GroupUserTypes;
use App\Models\Social\ConversationGroupUser;
use App\Repositories\Interfaces\Social\ConversationGroupUserInterface;

class ConversationGroupUserRepository implements ConversationGroupUserInterface{


    public function insert($data)
    {
        return ConversationGroupUser::insert($data);
    }

    public function create($data)
    {
        return ConversationGroupUser::create($data);
    }

    public function checkConversationUsers($usersIds, $conversationId)
    {
        return ConversationGroupUser::whereIn('user_id',$usersIds)
                                    ->where('conversation_id',$conversationId)
                                    ->get();
    }

    public function getGroupAdmin($conversationId)
    {
        return ConversationGroupUser::where('conversation_id',$conversationId)
                                    ->where('type',GroupUserTypes::admin)
                                    ->first();
    }

    public function getConversationUsers($conversationId)
    {
        return ConversationGroupUser::where('conversation_id',$conversationId)
        ->join('users','users.id','=','conversation_group_users.user_id')
        ->select('users.first_name','users.last_name','users.profile_pic','conversation_group_users.*')
        ->get();
    }
}
