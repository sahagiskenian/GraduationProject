<?php

namespace App\Repositories\Implementations\Social;

use App\Enums\ConversationTypes;
use App\Models\Social\Conversation;
use App\Models\Social\ConversationGroupHeader;
use App\Repositories\Interfaces\Social\ConversationInterface;

class ConversationRepository implements ConversationInterface
{


    public function get($userId)
    {
        return  Conversation::where('sender_id', $userId)
            ->orWhere('receiver_id', $userId)
            ->orWhereHas('conversationGroupUsers', function ($conversationGroupUser) use ($userId) {
                $conversationGroupUser->where('user_id', $userId);
            })
            ->with('lastMessage.sender')
            ->with(['convesationGroupHeader', 'sender', 'receiver'])
            ->withCount('unReadConversationMessageReceiver')
            ->get()
            ->sortByDesc([
                fn ($a, $b) => $a['lastMessage']['id'] < $b['lastMessage']['id'],
            ])->values()->all();
    }

    public function create($data)
    {
        return Conversation::create($data);
    }

    public function checkConversationUser($conversationId, $userId)
    {
        return Conversation::where(function ($conversation) use ($conversationId, $userId) {
            $conversation->where('id', $conversationId)
                ->where(function ($query) use ($userId) {
                    $query->where('sender_id', $userId)
                        ->orWhere('receiver_id', $userId);
                });
        })->orWhereHas('conversationGroupUsers', function ($conversationGroupUser) use ($userId, $conversationId) {
            $conversationGroupUser->where('user_id', $userId)
                ->where('conversation_id', $conversationId);
        })->first();
    }

    public function search($filter)
    {
        return Conversation::when(!empty($filter['receiver_id']), function ($convesration) use ($filter) {
            $convesration->where(function ($query) use ($filter) {
                $query->where('receiver_id', $filter['receiver_id'])
                    ->where('sender_id', $filter['sender_id']);
            })
                ->orWhere(function ($query) use ($filter) {
                    $query->where('receiver_id', $filter['sender_id'])
                        ->where('sender_id', $filter['receiver_id']);
                })
                ->where('type', ConversationTypes::single);
        }, function ($conversation) use ($filter) {
            $conversation->where(function($group) use ($filter)
            {
                $group->where('type', ConversationTypes::group)
                ->whereHas('conversationGroupUsers', function ($conversationGroupUsers) use ($filter) {
                    $conversationGroupUsers->where('conversation_id', $filter['conversation_id'])
                        ->where('user_id', $filter['sender_id']);
                });
            })->orWhere(function ($single) use ($filter) {
                    $single->where('id', $filter['conversation_id'])
                        ->where('type', ConversationTypes::single)
                        ->where(function ($query) use ($filter) {
                            $query->where('sender_id', $filter['sender_id'])
                                ->orWhere('receiver_id', $filter['sender_id']);
                        });
                });
        })->first();
    }

    public function findByGenerationId($generationId)
    {
        return Conversation::where('generation_id',$generationId)
                            ->first();
    }

    public function createHeader($data)
    {
        return ConversationGroupHeader::create($data);
    }
}
