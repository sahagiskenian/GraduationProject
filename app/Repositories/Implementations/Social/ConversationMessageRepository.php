<?php

namespace App\Repositories\Implementations\Social;


use App\Models\Social\ConversationGroupUser;
use App\Models\Social\ConversationMessage;
use App\Models\Social\ConversationMessageReciever;
use App\Models\User\User;
use App\Notifications\ConversationMessageNotification;
use App\Repositories\Interfaces\Social\ConversationMessageInterface;
use Illuminate\Support\Facades\Notification;

class ConversationMessageRepository implements ConversationMessageInterface{

    public function get($conversationId,$userId)
    {
        return ConversationMessage::where('conversation_id',$conversationId)
        ->where(function($conversationMessage) use ($userId)
        {
            $conversationMessage->where('sender_id',$userId)
            ->orWhereHas('conversationMessageReceivers',function($conversationMessageReceiver) use ($userId)
        {
            $conversationMessageReceiver->where('receiver_id',$userId);
        });
        })->with('sender')
        ->latest()
        ->simplePaginate();
    }


    public function createSingleMessage($data)
    {
        $receiverId = $data['receiver_id'];
        unset($recieverId);
        $conversationMessage = ConversationMessage::create($data);
        $conversationMessage->refresh()->conversationMessageReceivers()->create([
            'receiver_id' => $receiverId
        ]);

         if (empty($conversationMessage))
          return null;
          $conversationMessage->sender;
          $user = User::where('id',$data['receiver_id'])->first();
          $user->notify(new ConversationMessageNotification($conversationMessage));
        return $this->latestMessage($data['conversation_id']);

    }

    public function read($conversationId , $recieverId)
    {
        return ConversationMessageReciever::whereHas('conversationMessage',function($conversationMessage) use ($conversationId)
        {
            $conversationMessage->where('conversation_id',$conversationId);

        })
        ->where('receiver_id',$recieverId)
        ->whereNull('conversation_message_receivers.read_at')
        ->update([
            'read_at' => now()
        ]);
    }

    public function createGroupMessage($conversationId,$message,$senderId = null)
    {
        $groupUsersIds = ConversationGroupUser::where('conversation_id',$conversationId)
                                ->where('user_id','!=',$senderId)
                                ->pluck('user_id');

        $conversationMessages = array();
        $conversationMessage['created_at'] = now();
        foreach($groupUsersIds as $groupUserId)
        {
            $conversationMessage['receiver_id'] = $groupUserId;
            array_push($conversationMessages , $conversationMessage);
        }

        $conversationMessageResult = ConversationMessage::create([
            'description' => $message,
            'sender_id' => $senderId,
            'conversation_id' => $conversationId,
        ]);
        if(empty($conversationMessageResult))
        return null;
        $convesrtionGroupMessage = $conversationMessageResult->refresh()->conversationMessageReceivers()->createMany($conversationMessages);
        $conversationMessageResult->sender;
        $users = User::whereIn('id',$groupUsersIds)->get();
        Notification::send($users,new ConversationMessageNotification($conversationMessageResult));
        return (empty($convesrtionGroupMessage)) ? null
        : $this->latestMessage($conversationId);
    }

    public function latestMessage($conversationId)
    {
        return ConversationMessage::where('conversation_id',$conversationId)
                                    ->with('sender')
                                    ->latest()
                                    ->first();
    }


}
