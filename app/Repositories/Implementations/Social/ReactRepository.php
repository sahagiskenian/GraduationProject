<?php

namespace App\Repositories\Implementations\Social;

use App\Models\Social\React;
use App\Repositories\Interfaces\Social\ReactInterface;

class ReactRepository implements ReactInterface {


    public function create($data)
    {
        return React::create($data);
    }

    public function find($filter)
    {
        return React::where($filter)
        ->first();
    }

    public function updateOrCreate($filter,$data)
    {
        return React::updateOrCreate($filter,$data);
    }

    public function delete($filter)
    {
        return React::where($filter)
                    ->forceDelete();
    }
}
