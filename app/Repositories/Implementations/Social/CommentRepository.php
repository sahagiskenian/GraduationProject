<?php

namespace App\Repositories\Implementations\Social;

use App\Models\Social\Comment;
use App\Repositories\Interfaces\Social\CommentInterface;

class CommentRepository implements CommentInterface{


    public function create($data)
    {
        return Comment::create($data);
    }

    public function find($filter)
    {
        return Comment::where($filter)->first();
    }

    public function update($filter,$data)
    {
        return Comment::where($filter)
        ->update($data);
    }

    public function delete($filter)
    {
        return Comment::where($filter)
        ->delete();
    }
}
