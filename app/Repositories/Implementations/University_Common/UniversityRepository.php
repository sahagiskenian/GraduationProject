<?php

namespace App\Repositories\Implementations\University_Common;


use App\Repositories\Interfaces\University_Common\UniversityInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use Auth;
use App\Models\University_Common\University;
use Carbon\Carbon;
use Request;
use Hash;

class UniversityRepository implements UniversityInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }


    public function GetUniversities(){

            $university=University::with("colleges")->get();


            $response=["data"=>$university,
                       "message"=>"universities Retrieved Successfully",
                       "Status" => 200];

                return response($response,200);




    }





}
