<?php

namespace App\Repositories\Implementations\Feedback;



use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use Auth;
use App\Models\User\UserType;
use App\Models\College_Common\College;
use App\Models\College_Common\Department;
use App\Models\Feedback\Feedback;
use App\Repositories\Interfaces\Feedback\FeedbackInterface;
use Carbon\Carbon;
use Request;
use Hash;

class FeedbackRepository implements FeedbackInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }


    public function GetFeedbacks()
    {

    }


    public function AddFeedback($array)
    {
        $feedback=Feedback::create([
            "feedback_name"=>$array->feedback_name,
            "feedback_message"=>$array->feedback_message,
            "feedback_email"=>$array->feedback_email
        ]);

        $response=[
        "message"=>"Feedback Added Successfully",
        "Status" => 200];

        return response($response,200);

}




}
