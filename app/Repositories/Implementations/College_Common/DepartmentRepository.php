<?php

namespace App\Repositories\Implementations\College_Common;



use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use Auth;
use App\Models\User\UserType;
use App\Models\College_Common\College;
use App\Models\College_Common\Department;
use App\Models\Subject\Subject;
use App\Repositories\Interfaces\College_Common\DepartmentInterface;
use Carbon\Carbon;
use Request;
use Hash;

class DepartmentRepository implements DepartmentInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }



    public function GetDepartmentSubjects($id)
    {
        $subjects=Subject::with("department")->where("department_id",$id)->get();


        $response=["data"=>$subjects,
                   "message"=>"Subjects Retrieved Successfully",
                   "Status" => 200];

            return response($response,200);
    }







}
