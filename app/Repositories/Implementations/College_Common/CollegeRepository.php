<?php

namespace App\Repositories\Implementations\College_Common;


use App\Repositories\Interfaces\College_Common\CollegeInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use Auth;
use App\Models\User\UserType;
use App\Models\College_Common\College;
use App\Models\College_Common\Department;
use App\Models\College_Common\Generation;
use App\Models\College_Common\GenerationYear;
use App\Models\College_Common\Year;
use App\Models\University_Common\UniversityCalender;
use App\Models\User\User;
use Carbon\Carbon;
use Request;
use Hash;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use RealRashid\SweetAlert\Facades\Alert as FacadesAlert;

class CollegeRepository implements CollegeInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }


    public function GetUniversityColleges($id){

            $college=College::with("university")->where("university_id",$id)->get();


            $response=["data"=>$college,
                       "message"=>"Colleges Retrieved Successfully",
                       "Status" => 200];

                return response($response,200);




    }
    public function GetCollegeDeparments($id){
        $departments=Department::with("college")->where("college_id",$id)->get();


        $response=["data"=>$departments,
                   "message"=>"Departments Retrieved Successfully",
                   "Status" => 200];

            return response($response,200);
    }

    public function AddGeneration($array)
    {
        Generation::create([
            "name"=>$array->name,
            "entry_date"=>$array->entry_date,
            "graduation_date"=>$array->graduation_date,
            "description"=>$array->description,
            "student_num"=>$array->student_num,
            "college_id"=>FacadesAuth::user()->college_id
        ]);

        FacadesAlert::success(" تم      اضافة دفعة بنجاح   ");

        return redirect()->back();
    }

    public function GetGenerations()
    {
         $generations=Generation::with('college')->where('college_id',FacadesAuth::user()->college_id)->get();


         return view('admin.College_Common.Generation.CollegeGenerations')
                ->with('generations',$generations);
    }

    public function RemoveGeneration($id)
    {
         $generations=Generation::find($id)->delete();

         FacadesAlert::success(" تم حذف دفعة بنجاح   ");

         return redirect()->back();
    }



 public function GetGenerationYear()
 {
     $years=Year::where('college_id',FacadesAuth::user()->college_id)->get();
     $departments=Department::where('college_id',FacadesAuth::user()->college_id)->get();
     $universityCalenders=UniversityCalender::where('university_id',FacadesAuth::user()->university_id)->get();
     $generations=Generation::where('college_id',FacadesAuth::user()->college_id)->get();
     $generationYears=GenerationYear::with('year')->with('department')->with('generation')->with('university_calender')->get();

          return view('admin.College_Common.Generation.GenerationYear')
          ->with('years',$years)
          ->with('departments',$departments)
          ->with('universityCalenders',$universityCalenders)
          ->with('generations',$generations)
          ->with('generationYears',$generationYears);
 }

 public function AddGenerationYear($array)
 {
    GenerationYear::create([
        "year_id"=>$array->year_id,
        "department_id"=>$array->department_id,
        "generation_id"=>$array->generation_id,
        "university_calender_id"=>$array->university_calender_id,


    ]);

    FacadesAlert::success(" تم      اضافة سنة دفعة بنجاح   ");

    return redirect()->back();
 }

 public function RemoveGenerationYear($id)
 {

    $generations=GenerationYear::find($id)->delete();

    FacadesAlert::success(" تم حذف دفعة السنة بنجاح   ");

    return redirect()->back();
 }

 public function GetCollegeDetails($id)
 {/*
     $college=College::with('university')->with('departments')->with('generations')->with('years')->with('common_questions')->with(['users' => function ($query) {
        $query->where('user_type_id',3)->first();
    }])->where('id',$id)->get();*/
    $college=User::with(['college' => function ($query) use($id) {
        $query->with('university')->with('departments.subjects.files')->with('generations')->with('years')->with('common_questions')->with(['galleries.galleryusers.user' => function($comment)
        {
            $comment->with(['university','college','galleryUsers.gallery']);
        }])->where('id',$id);
    }])->where('user_type_id',3)->first();

    $response=["data"=>$college,
    "message"=>"College Retrieved Successfully",
    "Status" => 200];

    return response($response,200);
 }

}
