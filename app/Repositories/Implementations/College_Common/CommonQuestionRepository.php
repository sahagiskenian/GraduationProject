<?php

namespace App\Repositories\Implementations\College_Common;

use App\Http\Resources\ResponseCollection;
use App\Models\College_Common\CommonQuestion;
use Illuminate\Database\Eloquent\Model;

use App\Repositories\Interfaces\College_Common\CommonQuestionInterface;


class CommonQuestionRepository implements CommonQuestionInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }

  public function GetAllCommonQuestions()
  {
      $questions=CommonQuestion::with(['college' => function ($query) {
        $query->with(['university','specialization'])->get();
        }])->paginate(6);


      return response()->json(
          [
              "data"=>$questions,
              "message"=>"data retrieved successfully",
              "status"=>200
          ]
      );
  }

  public function GetCollegeCommonQuestions($id)
  {
    $questions=CommonQuestion::with(['college' => function ($query) {
        $query->with(['university','specialization'])->get();
        }])->where('college_id',$id)->paginate(6);


    return response()->json(
        [
            "data"=>$questions,
            "message"=>"data retrieved successfully",
            "status"=>200
        ]
    );
  }


}
