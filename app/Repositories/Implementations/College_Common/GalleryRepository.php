<?php

namespace App\Repositories\Implementations\College_Common;



use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use App\Http\Controllers\API\College_Common\CollegeController;
use Auth;
use App\Models\User\UserType;
use App\Models\College_Common\College;
use App\Models\College_Common\Department;
use App\Models\College_Common\Gallery;
use App\Models\College_Common\GalleryUser;
use App\Models\Subject\Subject;
use App\Models\User\User;
use App\Repositories\Interfaces\College_Common\DepartmentInterface;
use App\Repositories\Interfaces\College_Common\GalleryInterface;
use Carbon\Carbon;
use Request;
use Hash;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use PhpParser\Node\Stmt\Foreach_;
use RealRashid\SweetAlert\Facades\Alert as FacadesAlert;

class GalleryRepository implements GalleryInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }



    public function GetCollegeGallery($id)
    {
         $galleries=Gallery::with('university_calender')->with('department.college')->whereHas( 'department' , function ($query) use($id) {
            $query->with('college')->where('college_id',$id);
        } )->paginate(6);

        $response=["data"=>$galleries,
        "message"=>"Gallery Retrieved Successfully",
        "Status" => 200];

             return response($response,200);
    }



public function GetMyGallery()
{

    $ids=College::with(['departments' => function ($query) {
        $query;
        }])->where('id',FacadesAuth::user()->college_id)->get();
        $a=array();
       // dd($ids);
        foreach($ids[0]->departments as $department){
            array_push($a,$department->id);
        }

    //dd($a);
    $gallery=Gallery::with('department')->with('university_calender')->whereIn('department_id',$a)->get();

 /*   $ids=Gallery::with(['department' => function ($query) {
        $query->where('college_id',FacadesAuth::user()->college_id)->get();
        }])->get();
*/

    return view('admin.College_Common.Gallery.Galleries')
            ->with('galleries',$gallery);
}


public function RemoveFromMyGallery($id)
{
    Gallery::find($id)->delete();

   FacadesAlert::success('Removed From Gallery Succesfully');
    return redirect()->back();
}

public function RemoveStudentFromGallery($gallery_id,$user_id)
{
    GalleryUser::where('gallery_id',$gallery_id)->where('user_id',$user_id)->delete();

    FacadesAlert::success('Removed From Gallery Succesfully');
     return redirect()->back();
}
public function GetGalleryDetails($id){
$gallery=Gallery::with(['galleryusers' => function ($query) {
    $query->with('user')->get();
    }])->where('id',$id)->first();


$users=User::where('college_id',FacadesAuth::user()->college_id)->get();

return view('admin.College_Common.Gallery.EditGallery')
       ->with('gallery',$gallery)
       ->with('users',$users);
}

public function AddStudentToGallery($array)
{
    $gallery_student=GalleryUser::create([
        'user_id'=>$array->user_id,
        'gallery_id'=>$array->gallery_id
    ]);

    FacadesAlert::success('Student Added To Gallery Succesfully');
    return redirect()->back();

}

public function EditGallery($array)
{
      $gallery=Gallery::find($array->id);
      $gallery->name=$array->name;
      $gallery->description=$array->description;
      $mainImage = $array->file('logo');
      if($mainImage) {

        $filename    = time().".".$mainImage->getClientOriginalExtension();

        $destinationPath = public_path('uploads/galleries/');
        $mainImage->move($destinationPath, $filename);

            $gallery->logo='uploads/galleries/'.$filename;
      }
      $gallery->save();
      FacadesAlert::success('  Gallery Edited Succesfully');
      return redirect()->back();
}


}
