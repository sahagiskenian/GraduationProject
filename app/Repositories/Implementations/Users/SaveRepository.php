<?php

namespace App\Repositories\Implementations\Users;



use App\Models\User\Save;

use App\Repositories\Interfaces\Users\SaveInterface;

use Kouja\ProjectAssistant\Helpers\ResponseHelper;


class SaveRepository implements SaveInterface
{



public function FindSave($id)
{
    return Save::find($id);
}

public function CreateSave($data)
{
    return Save::create($data);
}


public function RemoveSave($id)
{
    return Save::where('id',$id)
    ->delete();
}


}
