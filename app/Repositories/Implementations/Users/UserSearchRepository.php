<?php

namespace App\Repositories\Implementations\Users;


use App\Repositories\Interfaces\Users\UserTypeInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use Auth;
use App\Models\User\UserType;
use App\Models\User\User;
use App\Models\University_Common\University;
use App\Models\College_Common\College;
use App\Models\User\UserSearch;
use App\Repositories\Interfaces\Users\UserSearchInterface;
use Carbon\Carbon;
use Request;
use Hash;
use Illuminate\Support\Facades\Hash as FacadesHash;
use RealRashid\SweetAlert\Facades\Alert as FacadesAlert;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;


class UserSearchRepository implements UserSearchInterface
{

    public function getUserSearchs(User $user)
    {
        $searchs=UserSearch::where('user_id',$user->id)->orderBy('created_at','desc')->get();


        return ResponseHelper::select($searchs);
    }

    public function postUserSearchs($array,User $user)
    {
        UserSearch::create([
            'user_id'=>$user->id,
            'search_text'=>$array
        ]);

      return  $this->getUserSearchs($user);
    }

    public function ClearSearchs(User $user)
    {
        $searchs=UserSearch::where('user_id',$user->id)->delete();


        return response([
            'message'=>'Searchs Cleared Successfully',
            'status'=>200
        ]);
    }


}
