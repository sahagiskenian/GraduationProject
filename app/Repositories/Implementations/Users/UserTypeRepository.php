<?php

namespace App\Repositories\Implementations\Users;


use App\Repositories\Interfaces\Users\UserTypeInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use Auth;
use App\Models\User\UserType;
use App\Models\User\User;
use App\Models\University_Common\University;
use App\Models\College_Common\College;
use Carbon\Carbon;
use Request;
use Hash;
use Illuminate\Support\Facades\Hash as FacadesHash;
use RealRashid\SweetAlert\Facades\Alert as FacadesAlert;

class UserTypeRepository implements UserTypeInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }

   /**
    * @return Collection
    */
   public function getUserTypes()
   {
    $userTypes=UserType::all();

   return view('admin.Users.UserType.UserTypes')
          ->with("userTypes",$userTypes)
          ->with("university",University::all());
   }

   public function AddAdmin($attributes){

  $admin=User::create($attributes);
  $admin->verified_at=Carbon::now();
  $admin->password=FacadesHash::make($attributes['password']);
  $admin->save();
  FacadesAlert::success("Admin Added Successfully");
  return redirect()->back();

   }

   public function RemoveAdmin($id){
$user=User::find($id)->delete();
FacadesAlert::success("Deleted Successully");
return redirect()->back();

   }

   public function AcceptStudent($id){
    User::where('id',$id)->update(['verified_at'=>Carbon::now()]);
    FacadesAlert::success("Student Accepted");
    return redirect()->back();

   }



   public function GetAdminsPage($id){
    if($id==5) {
        $users=User::where("user_type_id",$id)->where("verified_at","=",null)->get();
                   }
                   elseif($id==6){
                       $users= User::where("verified_at",'<>',null)->where("user_type_id",5)->get();
                   }
                   else {
        $users=User::with("university")->with("college")->where("user_type_id",$id)->get() ;
                   }



                return  view("admin.Users.UserType.AddAdmin")
                          ->with("user_id",$id)
                          ->with("users",$users)
                          ->with("university",University::all())
                          ->with("colleges",College::all());
   }






}
