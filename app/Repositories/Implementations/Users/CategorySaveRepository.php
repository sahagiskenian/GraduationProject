<?php

namespace App\Repositories\Implementations\Users;


use App\Repositories\Interfaces\Users\UserTypeInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use Auth;
use App\Models\User\UserType;
use App\Models\User\User;
use App\Models\University_Common\University;
use App\Models\College_Common\College;
use App\Models\User\CategorySave;
use App\Models\User\UserSearch;
use App\Repositories\Interfaces\Users\CategorySaveInterface;
use App\Repositories\Interfaces\Users\UserSearchInterface;
use Carbon\Carbon;
use Request;
use Hash;
use Illuminate\Support\Facades\Hash as FacadesHash;
use RealRashid\SweetAlert\Facades\Alert as FacadesAlert;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;


class CategorySaveRepository implements CategorySaveInterface
{

public function GetUserCategorySave($id){

    return CategorySave::with('saves.post')->where('user_id',$id)
    ->get();
}

public function FindCategorySave($id)
{
    return CategorySave::find($id);
}

public function CreateCategorySave($data)
{
    return CategorySave::create($data);
}


public function RemoveCategorySave($id)
{
    return CategorySave::where('id',$id)
    ->delete();
}


}
