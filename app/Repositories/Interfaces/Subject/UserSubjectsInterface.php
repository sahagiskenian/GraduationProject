<?php

namespace App\Repositories\Interfaces\Subject;

interface UserSubjectsInterface {

    public function checkUserSubject($userId,$subjectId);

    public function create($data);
}
