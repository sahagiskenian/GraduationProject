<?php


namespace App\Repositories\Interfaces\Subject;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface SubjectInterface
{

   public function AddSubject($array);


   public function RemoveSubject($id);

   public function GetSubjects();

   public function AddRelatedSubjects($array);

   public function GetRelatedSubjects();

   public function RemoveRelatedSubjects($id);

   public function checkUserSubject(User $user,$subjectId);
}
