<?php


namespace App\Repositories\Interfaces\Subject;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface FileInterface
{

   public function AddFile($array);


   public function RemoveFile($id);

   public function GetFiles();

}
