<?php


namespace App\Repositories\Interfaces\Social;

interface CommentInterface{

    public function create($data);

     public function update($filter,$data);

     public function delete($filter);

    // public function get($filter);

     public function find($filter);

}
