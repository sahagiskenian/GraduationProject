<?php

namespace App\Repositories\Interfaces\Social;

interface ConversationInterface{

     public function get($userId);

    // public function update();

    // public function delete();

    // public function find();

    public function create($data);

    public function createHeader($data);

    public function checkConversationUser($conversationId,$userId);

    public function search($filter);

    public function findByGenerationId($generationId);

}
