<?php

namespace App\Repositories\Interfaces\Social;


interface ConversationGroupUserInterface {

    public function insert($data);

    public function create($data);

    public function checkConversationUsers($usersIds,$conversationId);

    public function getGroupAdmin($conversationId);

}
