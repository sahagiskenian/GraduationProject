<?php


namespace App\Repositories\Interfaces\Social;

interface RateInterface {

     public function create($data);

    // public function update($filter,$data);

     public function delete($filter);

     public function find($filter);

     public function updateOrCreate($filter,$data);

}
