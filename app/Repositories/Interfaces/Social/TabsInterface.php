<?php


namespace App\Repositories\Interfaces\Social;

use App\Models\User\User;
use Facade\Ignition\Tabs\Tab;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface TabsInterface
{
  public function GetTabs($university_id=null,$college_id=null,$specialization_id=null,$generation_year_id=null);

  public function GetUserTabs(User $user);

  public function checkUserTab(User $user ,$tabId);

  public function AddTab($array);

  public function RemoveTab($id);

}
