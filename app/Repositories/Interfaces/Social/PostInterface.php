<?php


namespace App\Repositories\Interfaces\Social;

use App\Models\User\User;

interface PostInterface
{
   public function create($array);

   public function update($id,$array);

   public function delete($id);

   public function find($filter);

   public function get($filter);

   public function paginate($filter);

   public function search($text,$tab_id);

   public function getGeneralQuestions(User $user);

   public function getGeneralQuestionFilters(User $user);

   public function getUserYearSemesterSubject(User $user ,$yearId ,$semesterId);

}
