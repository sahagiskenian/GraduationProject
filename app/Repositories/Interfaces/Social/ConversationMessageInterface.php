<?php


namespace App\Repositories\Interfaces\Social;


interface ConversationMessageInterface {

    public function get($conversationId,$recieverId);

    public function createSingleMessage($data);

    public function createGroupMessage($conversationId,$conversationMessage,$senderId = null);

    public function read($conversationId,$recieverId);


}
