<?php


namespace App\Repositories\Interfaces\University_Common;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Request;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface UniversityInterface
{

   public function GetUniversities();



}
