<?php


namespace App\Repositories\Interfaces\College_Common;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Request;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface CommonQuestionInterface
{

   public function GetAllCommonQuestions();

   public function GetCollegeCommonQuestions($id);

}
