<?php


namespace App\Repositories\Interfaces\College_Common;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Request;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface GalleryInterface
{
//API
public function GetCollegeGallery($id);
//Dashboard
public function GetMyGallery();

public function RemoveFromMyGallery($id);

public function GetGalleryDetails($id);

public function AddStudentToGallery($array);

public function EditGallery($array);

public function RemoveStudentFromGallery($gallery_id,$user_id);
}
