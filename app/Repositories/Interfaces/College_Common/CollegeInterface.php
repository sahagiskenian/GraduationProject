<?php


namespace App\Repositories\Interfaces\College_Common;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Request;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface CollegeInterface
{
   public function GetCollegeDetails($id);

   public function GetUniversityColleges($id);

   public function GetCollegeDeparments($id);

   public function AddGeneration($array);

   public function GetGenerations();

   public function RemoveGeneration($id);

   public function GetGenerationYear();

   public function AddGenerationYear($array);

   public function RemoveGenerationYear($id);

}
