<?php


namespace App\Repositories\Interfaces\Feedback;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Request;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface FeedbackInterface
{

   public function GetFeedbacks();

   public function AddFeedback($array);


}
