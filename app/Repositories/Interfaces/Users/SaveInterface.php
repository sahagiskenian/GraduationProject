<?php


namespace App\Repositories\Interfaces\Users;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Request;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface SaveInterface
{



   public function FindSave($id);

   public function CreateSave($data);

//   public function postUserSearchs($array,User $user);

   public function RemoveSave($id);


}
