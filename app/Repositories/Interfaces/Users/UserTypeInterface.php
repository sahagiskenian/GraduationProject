<?php


namespace App\Repositories\Interfaces\Users;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Request;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface UserTypeInterface
{

   public function getUserTypes();

   public function AddAdmin($attributes);

   public function RemoveAdmin($id);

   public function AcceptStudent($id);
//returns in order of the id
   public function GetAdminsPage($id);

}
