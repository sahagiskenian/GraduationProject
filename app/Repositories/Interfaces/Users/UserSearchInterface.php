<?php


namespace App\Repositories\Interfaces\Users;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Request;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface UserSearchInterface
{

   public function getUserSearchs(User $user);

   public function postUserSearchs($array,User $user);

   public function ClearSearchs(User $user);


}
