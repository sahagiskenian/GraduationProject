<?php

namespace App\Notifications;

use App\Models\Social\ConversationMessage;
use App\Models\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use Kutia\Larafirebase\Facades\Larafirebase;

class ConversationMessageNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $conversationMessage;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ConversationMessage $conversationMessage)
    {
        $this->conversationMessage = $conversationMessage;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['firebase'];
    }

    public function toFirebase($notifiable)
    {
        $deviceTokens = $notifiable->userTokens;
        $deviceTokens = $deviceTokens->pluck('token')->toArray();
        $data =  Larafirebase::withTitle($this->conversationMessage->sender->first_name.' '.$this->conversationMessage->sender->last_name)
            ->withBody($this->conversationMessage)
            ->sendMessage($deviceTokens);
            Log::info(json_encode($data));
    }


}
