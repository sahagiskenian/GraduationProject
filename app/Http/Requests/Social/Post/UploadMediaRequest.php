<?php


namespace App\Http\Requests\Social\Post;


use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class UploadMediaRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => ['required','file','max:10240'],
        ];
    }


}
