<?php

namespace App\Http\Requests\Social\Post;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class CreatePostRequest extends BaseFormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => ['required','min:1','max:50'],
            'description' => ['required','min:5','max:512'],
            'subject_id' => ['nullable',Rule::requiredIf(!filled($this->tab_id)),'integer'],
            'tab_id' => ['nullable',Rule::requiredIf(!filled($this->subject_id)),'integer'],
        ];
        if(filled($this->tab_id))
        unset($rules['subject_id']);
        return $rules;
    }
}
