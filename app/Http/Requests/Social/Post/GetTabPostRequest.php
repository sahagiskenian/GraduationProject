<?php

namespace App\Http\Requests\Social\Post;

use Illuminate\Foundation\Http\FormRequest;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class GetTabPostRequest extends BaseFormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tab_id' => ['required','integer']
        ];
    }
}
