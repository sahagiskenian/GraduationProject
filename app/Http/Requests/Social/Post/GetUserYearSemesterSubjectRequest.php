<?php


namespace App\Http\Requests\Social\Post;


use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class GetUserYearSemesterSubjectRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'year_id' => ['required','integer'],
            'semester_id' => ['required','integer']
        ];
    }


}
