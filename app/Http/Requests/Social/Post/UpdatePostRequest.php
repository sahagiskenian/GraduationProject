<?php


namespace App\Http\Requests\Social\Post;


use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class UpdatePostRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['nullable','min:1','max:50'],
            'description' => ['nullable','min:5','max:512'],
            'post_id' => ['required','integer']
        ];
    }


}
