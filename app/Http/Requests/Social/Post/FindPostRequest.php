<?php


namespace App\Http\Requests\Social\Post;


use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class FindPostRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post_id' => ['required','integer']
        ];
    }


}
