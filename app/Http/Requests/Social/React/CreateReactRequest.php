<?php


namespace App\Http\Requests\Social\React;

use App\Enums\ReactTypes;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class CreateReactRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'comment_id' => ['nullable',Rule::requiredIf(!filled($this->post_id)),'integer'],
            'post_id' => ['nullable',Rule::requiredIf(!filled($this->comment_id)),'integer'],
            'type' => ['required',new EnumValue(ReactTypes::class)]
        ];

        if(filled($this->comment_id))
        unset($rules['post_id']);
        return $rules;
    }


}
