<?php

namespace App\Http\Requests\Social\Comment;

use Illuminate\Foundation\Http\FormRequest;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class UpdateCommentRequest extends BaseFormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment_id' => ['required','integer'],
            'description' => ['required','string'],
        ];
    }
}
