<?php


namespace App\Http\Requests\Social\Comment;

use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class CreateCommentRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'comment_id' => ['nullable',Rule::requiredIf(!filled($this->post_id)),'integer'],
            'post_id' => ['nullable',Rule::requiredIf(!filled($this->comment_id)),'integer'],
            'description' => ['required','string'],
        ];

        if(filled($this->comment_id))
        unset($rules['post_id']);
        return $rules;
    }



}
