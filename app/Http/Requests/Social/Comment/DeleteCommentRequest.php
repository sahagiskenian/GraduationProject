<?php


namespace App\Http\Requests\Social\Comment;


use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class DeleteCommentRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment_id' => ['required','integer'],
        ];
    }


}
