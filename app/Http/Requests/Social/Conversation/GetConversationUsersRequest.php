<?php


namespace App\Http\Requests\Social\Conversation;


use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class GetConversationUsersRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'conversation_id' => ['required','integer'],
        ];
    }


}
