<?php


namespace App\Http\Requests\Social\Conversation;

use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class AddUsersToGroupRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_ids' => ['array','required','min:1'],
            'user_ids.*' => ['integer','required','distinct',Rule::notIn([$this->user()->id])],
        ];
    }


}
