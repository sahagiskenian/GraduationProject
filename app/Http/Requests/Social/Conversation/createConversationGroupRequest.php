<?php


namespace App\Http\Requests\Social\Conversation;

use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class createConversationGroupRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_ids' => ['array','required','min:2'],
            'user_ids.*' => ['integer','required','distinct',Rule::notIn([$this->user()->id])],
            'title' => ['required','string'],
            'img' => ['url','nullable']
        ];
    }


}
