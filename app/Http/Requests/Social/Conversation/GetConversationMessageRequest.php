<?php


namespace App\Http\Requests\Social\Conversation;


use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class GetConversationMessageRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'conversation_id' => ['required','integer']
        ];
    }


}
