<?php


namespace App\Http\Requests\Social\Conversation;

use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class createConversationMessageRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'conversation_id' => [Rule::requiredIf(empty($this->receiver_id)),'integer'],
            'receiver_id' => [Rule::requiredIf(empty($this->conversation_id)),'integer',Rule::exists('users','id'), Rule::notIn([$this->user()->id])],
            'message' => ['required','string']
        ];

        if(!empty($this->conversation_id))
        unset($rules['receiver_id']);

        return $rules;
    }


}
