<?php


namespace App\Http\Requests\Social\Rate;

use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class CreateRateRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'comment_id' => ['nullable',Rule::requiredIf(!filled($this->post_id)),'integer'],
            'post_id' => ['nullable',Rule::requiredIf(!filled($this->comment_id)),'integer'],
            'rate' => ['required','integer','min:-1','max:1']
        ];

        if(filled($this->comment_id))
        unset($rules['post_id']);
        return $rules;
    }


}
