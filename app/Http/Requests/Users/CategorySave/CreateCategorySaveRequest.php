<?php

namespace App\Http\Requests\Users\CategorySave;

use Illuminate\Foundation\Http\FormRequest;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class CreateCategorySaveRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //

            'name'=>['required','min:2','max:50']
        ];
    }
}
