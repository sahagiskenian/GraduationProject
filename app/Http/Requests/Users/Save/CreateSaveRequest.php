<?php

namespace App\Http\Requests\Users\Save;

use Illuminate\Foundation\Http\FormRequest;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class CreateSaveRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'post_id'=>['required'],
            'category_save_id'=>['required']
        ];
    }
}
