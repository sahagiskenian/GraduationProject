<?php

namespace App\Http\Controllers\Admin\Feedback;

use App\Http\Controllers\Controller;
use App\Models\Feedback\Feedback;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class FeedbackController extends Controller
{
    //

    public function GetFeedbacks(){

        $feedbacks=Feedback::orderBy('created_at')->get();

        return view('admin.Feedback.feedbacks')
               ->with('feedbacks',$feedbacks);
    }

    public function RemoveFeedback($id){

        Feedback::find($id)->delete();

Alert::success("Feedback Removed Successfully");

        return redirect()->back();
    }
}
