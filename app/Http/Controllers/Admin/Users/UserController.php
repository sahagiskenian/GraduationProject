<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\Users\UserTypeInterface;
use App\Models\User\User;
class UserController extends Controller
{
    //
    private $userTypeRepository;

    public function __construct(UserTypeInterface $userTypeRepository)
    {
        $this->userTypeRepository = $userTypeRepository;
    }

    public function getUserTypes(){
        return  $this->userTypeRepository->getUserTypes();
    }

    public function addAdmin($id){
        return   $this->userTypeRepository->GetAdminsPage($id);
    }
    public function createAdmin(Request $request){
        return   $this->userTypeRepository->addAdmin($request->all());
    }
 //This Function Removes Or Refuses The User
    public function RemoveAdmin($id){
        return   $this->userTypeRepository->RemoveAdmin($id);
    }
    //This Function Accepts The Student
    public function AcceptStudent($id){
        return   $this->userTypeRepository->AcceptStudent($id);

       }

}
