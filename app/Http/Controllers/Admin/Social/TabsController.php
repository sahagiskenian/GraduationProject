<?php

namespace App\Http\Controllers\Admin\Social;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\Social\TabsInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class TabsController extends Controller
{
    //
    private $tabsRepository;

    public function __construct(TabsInterface $tabsRepository)
    {
        $this->tabsRepository = $tabsRepository;
    }


    public function GetTabs(){
        if(Auth::user()->user_type_id==2){
            return $this->tabsRepository->GetTabs(Auth::user()->university_id);
        }
        if(Auth::user()->user_type_id==3){
            return $this->tabsRepository->GetTabs(null,Auth::user()->college_id,Auth::user()->college->specialization_id);
        }

    }

 public function AddTab(Request $request){


    return $this->tabsRepository->AddTab($request);
 }

 public function RemoveTab($id){


    return $this->tabsRepository->RemoveTab($id);


 }

}
