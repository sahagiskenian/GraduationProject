<?php

namespace App\Http\Controllers\Admin\Subjects;

use App\Http\Controllers\Controller;
use App\Models\College_Common\College;
use App\Models\College_Common\Department;
use App\Models\College_Common\Year;
use App\Models\Subject\Subject;
use App\Models\University_Common\Semester;
use App\Repositories\Interfaces\Subject\SubjectInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubjectController extends Controller
{
    //
    private $SubjectRepository;

    public function __construct(SubjectInterface $SubjectRepository)
    {
        $this->SubjectRepository = $SubjectRepository;
    }
    public function GetSubjects(){
        $college=   $this->SubjectRepository->GetSubjects();

    return view('admin.Subjects.Subject.AllSubjects')
           ->with("colleges",$college);
    }

    public function AddSubject(Request $request ) {
        if($request->isMethod('post')){
       return     $this->SubjectRepository->AddSubject($request);
      }

      if($request->isMethod('get')){

        $departments=Department::where('college_id',Auth::user()->college_id)->get();
        $years=Year::where('college_id',Auth::user()->college_id)->get();
        $semesters=Semester::where('university_id',Auth::user()->university_id)->get();
        return view('admin.Subjects.Subject.AddSubject')
        ->with('departments',$departments)
        ->with('years',$years)
        ->with('semesters',$semesters);
      }


    }
    public function RemoveSubject($id){

     return   $this->SubjectRepository->RemoveSubject($id);


    }
    public function RelatedSubjects(Request $request){
        if($request->isMethod('post')){
            return     $this->SubjectRepository->AddRelatedSubjects($request);
           }

           if($request->isMethod('get')){

            return $this->SubjectRepository->GetRelatedSubjects();
           }

    }
    public function RemoveRelatedSubjects($id){

    return $this->SubjectRepository->RemoveRelatedSubjects($id);
    }
}
