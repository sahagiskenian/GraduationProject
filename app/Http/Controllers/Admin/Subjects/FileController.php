<?php

namespace App\Http\Controllers\Admin\Subjects;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\Subject\FileInterface;
use Illuminate\Http\Request;

class FileController extends Controller
{
    //
    private $FileRepository;

    public function __construct(FileInterface $FileRepository)
    {
        $this->FileRepository = $FileRepository;
    }


    public function GetFiles(){

        return $this->FileRepository->GetFiles();

    }

    public function AddFile(Request $request){

        return $this->FileRepository->AddFile($request);

    }

    public function RemoveFile($id){

        return $this->FileRepository->RemoveFile($id);

    }
}
