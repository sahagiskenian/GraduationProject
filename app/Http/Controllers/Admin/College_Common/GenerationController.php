<?php

namespace App\Http\Controllers\Admin\College_Common;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\College_Common\CollegeInterface;
use Illuminate\Http\Request;

class GenerationController extends Controller
{
    //
    private $collegeInterface;

    public function __construct(CollegeInterface $collegeInterface)
    {
        $this->collegeInterface = $collegeInterface;
    }

    public function GetGenerations(){
        return $this->collegeInterface->GetGenerations();
    }
    public function AddGeneration(Request $request){
        return $this->collegeInterface->AddGeneration($request);
    }
    public function RemoveGeneration($id){
        return $this->collegeInterface->RemoveGeneration($id);
    }
}
