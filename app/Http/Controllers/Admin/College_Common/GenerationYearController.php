<?php

namespace App\Http\Controllers\Admin\College_Common;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\College_Common\CollegeInterface;
use Illuminate\Http\Request;

class GenerationYearController extends Controller
{
    //
    private $collegeInterface;

    public function __construct(CollegeInterface $collegeInterface)
    {
        $this->collegeInterface = $collegeInterface;
    }

    public function GetGenerationYear(){
        return $this->collegeInterface->GetGenerationYear();
    }
    public function AddGenerationYear(Request $request){
        return $this->collegeInterface->AddGenerationYear($request);
    }
    public function RemoveGenerationYear($id){
        return $this->collegeInterface->RemoveGenerationYear($id);
    }
}
