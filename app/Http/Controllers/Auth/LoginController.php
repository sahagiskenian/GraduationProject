<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\User\User;
use Alert;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

public function getLogin(){

    return view("auth.login");
}

public function authenticate(Request $request)
{

    $credentials = $request->only('email', 'password');
    $user=User::where('email','=',$request->email)->first();
    if($user ==null  ) {
        Alert::error('Email Mismatch' );
        return redirect('/login');
    }
    if($user->verified_at!=null) {


     if (Auth::attempt($credentials)) {
        // Authentication passed...
        return redirect('/admin');
    }
else {
    Alert::error("Password Mismatch");
    return redirect('/login');
}

}
else {
    Alert::error("Not Verified Yet");
    return redirect('/login');
}
}
}
