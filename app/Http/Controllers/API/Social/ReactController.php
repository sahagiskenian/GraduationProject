<?php

namespace App\Http\Controllers\API\Social;

use App\Http\Controllers\Controller;
use App\Http\Requests\Social\Rate\CreateRateRequest;
use App\Http\Requests\Social\React\CreateReactRequest;
use App\Repositories\Interfaces\Social\CommentInterface;
use App\Repositories\Interfaces\Social\PostInterface;
use App\Repositories\Interfaces\Social\RateInterface;
use App\Repositories\Interfaces\Social\ReactInterface;
use App\Repositories\Interfaces\Social\TabsInterface;
use App\Repositories\Interfaces\Subject\SubjectInterface;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class ReactController extends Controller {

   private $reactRepository,$commentRepository,$postRepository,$tabsRepository,$subjectRepository;
   public function __construct(ReactInterface $reactRepository,CommentInterface $commentRepository,PostInterface $postRepository,TabsInterface $tabsRepository,SubjectInterface $subjectRepository)
   {
        $this->reactRepository = $reactRepository;
        $this->postRepository = $postRepository;
        $this->commentRepository = $commentRepository;
        $this->subjectRepository = $subjectRepository;
        $this->tabsRepository = $tabsRepository;
   }

   public function react(CreateReactRequest $request)
   {
       $user = $request->user();
       $reactData = $request->validated();
       $reactData['generation_user_year_id'] = $user->currentGenrationUserYear->id;
       $reactData['user_id'] = $user->id;


       if(empty($reactData['post_id']))
       {
           $comment = $this->commentRepository->find(['id' => $reactData['comment_id']]);
            $post = empty($comment) ? null :
            ( (empty($comment->comment_id)) ? $comment->post :  optional($comment->comment)->post);
       }else $post = $this->postRepository->find(['id' => $reactData['post_id']]);


       if(empty($post))
       return ResponseHelper::DataNotFound();

       $authResult = (!empty($post->tab_id)) ?
       $this->tabsRepository->checkUserTab($user, $post->tab_id)
     : $this->subjectRepository->checkUserSubject($user, $post->subject_id);
        if (empty($authResult))
            return ResponseHelper::authorizationFail();

       $reactFilter = (!empty($reactData['post_id'])) ? ['post_id' => $reactData['post_id']]
       : ['comment_id' => $reactData['comment_id']];
       $reactFilter['user_id'] = $user->id;

       $oldReact = $this->reactRepository->find($reactFilter);

       if(!empty($oldReact) && $oldReact->type == $reactData['type'])
        {
            $deleteReactResult = $this->reactRepository->delete($reactFilter);
                if(empty($deleteReactResult))
                return ResponseHelper::operationFail();

            return ResponseHelper::delete();
        }
       $react = $this->reactRepository->updateOrCreate($reactFilter,$reactData);

       if(empty($react))
        return ResponseHelper::operationFail();

        return ResponseHelper::insert('react successfuly');
   }

}
