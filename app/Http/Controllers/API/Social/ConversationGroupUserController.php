<?php

namespace App\Http\Controllers\API\Social;

use App\Http\Controllers\Controller;
use App\Http\Requests\Social\Conversation\AddUsersToGroupRequest;
use App\Http\Requests\Social\Conversation\GetConversationUsersRequest;
use App\Repositories\Implementations\Social\ConversationGroupUserRepository;
use App\Repositories\Interfaces\Social\ConversationInterface;
use App\Repositories\Interfaces\Social\ConversationMessageInterface;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class ConversationGroupUserController extends Controller
{
    protected $conversationGroupUserRepositroy,$conversationRespository,$conversationMessageRepository;
    public function __construct(ConversationGroupUserRepository $conversationGroupUserRepository,ConversationInterface $conversationRespository,ConversationMessageInterface $conversationMessageRepository)
    {
        $this->conversationGroupUserRepositroy = $conversationGroupUserRepository;
        $this->conversationRespository = $conversationRespository;
        $this->conversationMessageRepository = $conversationMessageRepository;
    }


    public function addUser(AddUsersToGroupRequest $request)
    {
        $data = $request->validated();
        $user = $request->user();
        $generationId = $user->currentGenrationUserYear->generationyear->generation_id;
        $conversation = $this->conversationRespository->findByGenerationId($generationId);

       if(empty($conversation))
       return ResponseHelper::DataNotFound();

       $groupAdmin = $this->conversationGroupUserRepositroy->getGroupAdmin($conversation->id);
       if($groupAdmin->user_id != $user->id)
       return ResponseHelper::authorizationFail();

       $usersIds = $data['user_ids'];
       $userCheckResult = $this->conversationGroupUserRepositroy->checkConversationUsers($usersIds,$conversation->id);
       if(count($userCheckResult) > 0)
       return ResponseHelper::AlreadyExists();

        $groupUsers = array();
        foreach($usersIds as $userId)
        {
            array_push($groupUsers,[
                'user_id' => $userId,
                'conversation_id' => $conversation->id,
            ]);
        }
        $addGroupUsersResult = $this->conversationGroupUserRepositroy->insert($groupUsers);
        if(empty($addGroupUsersResult))
        return ResponseHelper::operationFail();

        $adduserConversationMessageResult = $this->conversationMessageRepository->createGroupMessage($conversation->id,"new users joined");
        if(empty($addGroupUsersResult))
        return ResponseHelper::operationFail();

        return ResponseHelper::insert($adduserConversationMessageResult);
    }

    public function getUsers(GetConversationUsersRequest $request)
    {
        $user = $request->user();
        $data = $request->validated();
        $checkConvesationUserResult = $this->conversationGroupUserRepositroy->checkConversationUsers([$user->id],$data['conversation_id']);

        if(empty($checkConvesationUserResult))
        return ResponseHelper::authorizationFail();

        $users = $this->conversationGroupUserRepositroy->getConversationUsers($data['conversation_id']);
        return ResponseHelper::select($users);

    }

}

