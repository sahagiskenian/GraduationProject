<?php

namespace App\Http\Controllers\API\Social;

use App\Http\Controllers\Controller;
use App\Http\Requests\Social\Rate\CreateRateRequest;
use App\Repositories\Interfaces\Social\CommentInterface;
use App\Repositories\Interfaces\Social\PostInterface;
use App\Repositories\Interfaces\Social\RateInterface;
use App\Repositories\Interfaces\Social\TabsInterface;
use App\Repositories\Interfaces\Subject\SubjectInterface;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class RateController extends Controller
{

    private $rateRepository, $commentRepository, $postRepository, $tabsRespository, $subjectRepository;

    public function __construct(RateInterface $rateRepository, CommentInterface $commentRepository, PostInterface $postRepository, SubjectInterface $subjectRepository, TabsInterface $tabsRespository)
    {
        $this->rateRepository = $rateRepository;
        $this->postRepository = $postRepository;
        $this->commentRepository = $commentRepository;
        $this->tabsRespository = $tabsRespository;
        $this->subjectRepository = $subjectRepository;
    }

    public function rate(CreateRateRequest $request)
    {
        $user = $request->user();
        $rateData = $request->validated();
        $rateData['user_id'] = $user->id;
        $rateData['generation_user_year_id'] = $user->currentGenrationUserYear->id;

        if (empty($rateData['post_id'])) {
            $comment = $this->commentRepository->find(['id' => $rateData['comment_id']]);
            $post = empty($comment) ? null : ((empty($comment->comment_id)) ? $comment->post :  optional($comment->comment)->post);
        } else $post = $this->postRepository->find(['id' => $rateData['post_id']]);

        if (empty($post))
            return ResponseHelper::DataNotFound();

        $authResult = (!empty($post->tab_id)) ?
            $this->tabsRespository->checkUserTab($user, $post->tab_id)
          : $this->subjectRepository->checkUserSubject($user, $post->subject_id);
        if (empty($authResult))
            return ResponseHelper::authorizationFail();

        $rateFilter = (!empty($rateData['post_id'])) ? ['post_id' => $rateData['post_id']]
            : ['comment_id' => $rateData['comment_id']];
        $rateFilter['user_id'] = $user->id;

        $oldRate = $this->rateRepository->find($rateFilter);
        if ($rateData['rate'] == 0) {
            if (empty($oldRate))
                return ResponseHelper::AlreadyExists();

            $deleteRateResult = $this->rateRepository->delete($rateFilter);
            if (empty($deleteRateResult))
                return ResponseHelper::operationFail();

            return ResponseHelper::delete();
        }

        $rate = $this->rateRepository->updateOrCreate($rateFilter, $rateData);
        if (empty($rate))
            return ResponseHelper::operationFail();

        return ResponseHelper::insert('rated successfuly');
    }
}
