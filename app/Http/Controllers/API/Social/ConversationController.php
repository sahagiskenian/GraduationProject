<?php

namespace App\Http\Controllers\API\Social;

use App\Enums\ConversationTypes;
use App\Enums\GroupUserTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Social\Conversation\createConversationGroupRequest;
use App\Http\Requests\Social\Conversation\GetConversationRequest;
use App\Http\Resources\Social\ConversationCollection;
use App\Http\Resources\Social\ConversationResource;
use App\Models\Social\ConversationGroupUser;
use App\Repositories\Interfaces\Social\ConversationGroupUserInterface;
use App\Repositories\Interfaces\Social\ConversationInterface;
use App\Repositories\Interfaces\Social\ConversationMessageInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class ConversationController extends Controller{

    protected $conversationRepository,$conversationMessageRepository,$conversationGroupUserRepository;
    public function __construct(ConversationInterface $conversationRepository,ConversationMessageInterface $conversationMessageRepository,ConversationGroupUserInterface $conversationGroupUserRepository)
    {
        $this->conversationRepository = $conversationRepository;
        $this->conversationGroupUserRepository = $conversationGroupUserRepository;
        $this->conversationMessageRepository = $conversationMessageRepository;
    }

    public function get(GetConversationRequest $request)
    {
        $data = $request->validated();
        $user = $request->user();
        $converstions = $this->conversationRepository->get($user->id);
        $conversationCollection = new ConversationCollection($converstions);
        return ResponseHelper::select($conversationCollection);

    }

    public function createGroup(createConversationGroupRequest $request)
    {
        $data = $request->validated();
        $user = $request->user();
        $generationId = $user->currentGenrationUserYear->generationyear->generation_id;
         $conversation = $this->conversationRepository->findByGenerationId($generationId);

        if(!empty($conversation))
        return ResponseHelper::AlreadyExists();

        try{
            DB::beginTransaction();
            $conversation = $this->conversationRepository->create([
                'type' => ConversationTypes::group,
                'generation_id' => $generationId,
            ]);
            if(empty($conversation))
            return ResponseHelper::operationFail();

            $conversationGroupHeadrData = $request->only('title','img');
            $conversationGroupHeadrData['conversation_id'] = $conversation->id;
            $converstionHeader = $this->conversationRepository->createHeader($conversationGroupHeadrData);
            if(empty($converstionHeader)){
                DB::rollBack();
                return ResponseHelper::operationFail();
                }


            $conversationGroupUser = [
                'user_id' => $user->id,
                'type' => GroupUserTypes::admin,
                'conversation_id' => $conversation->id,
            ];

            $convesationGroupUsers = array();
            array_push($convesationGroupUsers,$conversationGroupUser);

            $conversationGroupUser['type'] = GroupUserTypes::user;
            foreach($data['user_ids'] as $userId)
            {
                $conversationGroupUser['user_id'] = $userId;
                array_push($convesationGroupUsers,$conversationGroupUser);
            }
            $conversationGroupUsers = $this->conversationGroupUserRepository->insert($convesationGroupUsers);
            if(empty($conversationGroupUsers)){
            DB::rollBack();
            return ResponseHelper::operationFail();
            }

            $converstionMessage = $this->conversationMessageRepository->createGroupMessage($conversation->id,"group created");
            if(empty($converstionMessage)){
                DB::rollBack();
            return ResponseHelper::operationFail();
            }
            DB::commit();
            return ResponseHelper::insert($converstionMessage);
        }catch(Exception $exception)
        {
            DB::rollBack();
            return ResponseHelper::operationFail($exception->getMessage());
        }

    }


    // public function delete(DeleteConvesrationRequest $request)
    // {

    // }

    // public function update(updateConvesationRequest $request)
    // {

    // }


}
