<?php

namespace App\Http\Controllers\API\Social;

use App\Http\Controllers\Controller;
use App\Http\Requests\Social\Comment\CreateCommentRequest;
use App\Http\Requests\Social\Comment\DeleteCommentRequest;
use App\Http\Requests\Social\Comment\GetRepliesRequest;
use App\Http\Requests\Social\Comment\UpdateCommentRequest;
use App\Repositories\Interfaces\Social\CommentInterface;
use App\Repositories\Interfaces\Social\PostInterface;
use App\Repositories\Interfaces\Social\TabsInterface;
use App\Repositories\Interfaces\Subject\SubjectInterface;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class CommentController extends Controller{

    protected $commentRepository,$postRepository,$subjectRepository,$tabsRepository;

    public function __construct(CommentInterface $commentInterface,PostInterface $postInterface,SubjectInterface $subjectRepository,TabsInterface $tabsRepository)
    {
        $this->commentRepository = $commentInterface;
        $this->postRepository = $postInterface;
        $this->subjectRepository = $subjectRepository;
        $this->tabsRepository = $tabsRepository;
    }


    public function create(CreateCommentRequest $request)
    {
        $user = $request->user();

        $commentData = $request->validated();
        $commentData['user_id'] = $user->id;
        $commentData['generation_user_year_id'] = $user->currentGenrationUserYear->id;
        $post = (empty($commentData['post_id'])) ? optional($this->commentRepository->find(['id' => $commentData['comment_id']]))->post
        : $this->postRepository->find(['id' => $commentData['post_id']]);

        if(empty($post))
        return ResponseHelper::DataNotFound();

        $authResult = (!empty($post->tab_id)) ?
         $this->tabsRepository->checkUserTab($user,$post->tab_id)
       : $this->subjectRepository->checkUserSubject($user,$post->subject_id);
        if(empty($authResult))
        return ResponseHelper::authorizationFail();

        $createdComment = $this->commentRepository->create($commentData);
        $createdComment->refresh();
        if(empty($createdComment))
        return ResponseHelper::operationFail();

        return ResponseHelper::insert($createdComment);
    }

    public function update(UpdateCommentRequest $request)
    {
        $commentData = $request->validated();
        $commentId = $commentData['comment_id'];
        unset($commentData['comment_id']);

        $comment = $this->commentRepository->find(['id' => $commentId]);
        if(empty($comment))
        return ResponseHelper::DataNotFound();

        $this->authorize('update',$comment);

        $updateCommentResult = $this->commentRepository
        ->update(['id' => $commentId],$commentData);

        if(empty($updateCommentResult))
        return ResponseHelper::operationFail();

        $comment = $comment->refresh();
        return ResponseHelper::update($comment);
    }

    public function delete(DeleteCommentRequest $request)
    {
        $user = $request->user();
        $commentData = $request->validated();

        $comment = $this->commentRepository->find(['id' => $commentData['comment_id']]);
        if(empty($comment))
        return ResponseHelper::DataNotFound();

        $this->authorize('delete',$comment);

        $deleteCommentResult = $this->commentRepository->delete(['id' => $commentData['comment_id']]);
        if(empty($deleteCommentResult))
        return ResponseHelper::operationFail();

        return ResponseHelper::delete();
    }

    public function getReplies(GetRepliesRequest $request)
    {
        $user = $request->user();
        $commentData = $request->validated();

        $comment = $this->commentRepository->find(['id' => $commentData['comment_id']]);
        if(empty($comment) || !empty($comment->comment_id))
        return ResponseHelper::DataNotFound();

        $post = $comment->post;

        $authResult = (!empty($post->tab_id)) ?
         $this->tabsRepository->checkUserTab($user,$post->tab_id)
       : $this->subjectRepository->checkUserSubject($user,$post->subject_id);
        if(empty($authResult))
        return ResponseHelper::authorizationFail();

        $replaies = $comment->comments;
        return ResponseHelper::select($replaies);
    }

}
