<?php

namespace App\Http\Controllers\API\Social;

use App\Enums\ConversationTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Social\Conversation\createConversationMessageRequest;
use App\Http\Requests\Social\Conversation\GetConversationMessageRequest;
use App\Repositories\Implementations\Social\ConversationMessageRepository;
use App\Repositories\Interfaces\Social\ConversationInterface;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class ConversationMessageController extends Controller{

    protected $conversatrionMessageRepository,$conversationRepositroy;

    public function __construct(ConversationInterface $conversationRepositroy,ConversationMessageRepository $conversationMessageRepository)
    {
        $this->conversationRepositroy = $conversationRepositroy;
        $this->conversatrionMessageRepository = $conversationMessageRepository;
    }


    public function get(GetConversationMessageRequest $request)
    {
        $data = $request->validated();
        $user = $request->user();
        $conversation = $this->conversationRepositroy->checkConversationUser($data['conversation_id'],$user->id);
        if(empty($conversation))
        return ResponseHelper::authorizationFail();

        $convestaionMessage = $this->conversatrionMessageRepository->get($data['conversation_id'],$user->id);
        $this->conversatrionMessageRepository->read($data['conversation_id'],$user->id);

        return ResponseHelper::select($convestaionMessage);

    }

    public function create(createConversationMessageRequest $request)
    {
        $data = $request->validated();
        $user = $request->user();
        $data['sender_id'] = $user->id;
        $conversation = $this->conversationRepositroy->search($data);

        if(empty($conversation) && !empty($data['receiver_id']))
        $conversation = $this->conversationRepositroy->create([
            'sender_id' => $user->id,
            'receiver_id' => $data['receiver_id'],
            'type' => ConversationTypes::single,
        ]);
        if(empty($conversation))
        return ResponseHelper::authorizationFail();
        $conversationMessage = ($conversation->type == ConversationTypes::single) ?
         $this->conversatrionMessageRepository->createSingleMessage([
             'sender_id' => $user->id,
             'receiver_id' => ($conversation->sender_id != $user->id) ? $conversation->sender_id : $conversation->receiver_id,
             'conversation_id' => $conversation->id,
             'description' => $data['message'],
         ])
         : $this->conversatrionMessageRepository->createGroupMessage($conversation->id,$data['message'],$user->id);
        if(empty($conversationMessage))
        return ResponseHelper::operationFail();

        return ResponseHelper::select($conversationMessage);
    }

}
