<?php

namespace App\Http\Controllers\API\Social;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\Social\TabsInterface;
use Illuminate\Http\Request;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class TabController extends Controller
{
    protected $tabRepository;

    public function __construct(TabsInterface $tabRepository)
    {
      $this->tabRepository = $tabRepository;
    }

    public function get(Request $request)
    {
        $user = $request->user();

        return ResponseHelper::select($this->tabRepository->GetUserTabs($user));
    }
}
