<?php

namespace App\Http\Controllers\API\Social;

use App\Http\Controllers\Controller;
use App\Http\Requests\Social\Post\CreateGeneralQuestionRequest;
use App\Http\Requests\Social\Post\CreatePostRequest;
use App\Http\Requests\Social\Post\DeletePostRequest;
use App\Http\Requests\Social\Post\FindPostRequest;
use App\Http\Requests\Social\Post\GetPostsRequest;
use App\Http\Requests\Social\Post\GetTabPostRequest;
use App\Http\Requests\Social\Post\GetUserYearSemesterSubjectRequest;
use App\Http\Requests\Social\Post\UpdatePostRequest;
use App\Http\Requests\Social\Post\UploadMediaRequest;
use App\Repositories\Implementations\Social\PostRepository;
use App\Repositories\Implementations\Subject\SubjectRepository;
use App\Repositories\Implementations\Subject\UserSubjectsRepository;
use App\Repositories\Interfaces\Social\PostInterface;
use App\Repositories\Interfaces\Social\TabsInterface;
use App\Repositories\Interfaces\Subject\UserSubjectsInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;
use Illuminate\Support\Str;

class PostController extends Controller{

    protected $postRepository,$tabsRepository;

    public function __construct(PostInterface $postRepository,TabsInterface $tabsRepository)
    {
        $this->postRepository = $postRepository;
        $this->tabsRepository = $tabsRepository;
    }

    public function search(Request $request){

        $searchResult= $this->postRepository->search($request->get('text'),$request->get('tab_id'));


        return ResponseHelper::select($searchResult);
        }



    public function create(CreatePostRequest $request,UserSubjectsRepository $userSubjectsRepositroy)
    {
        $postData = $request->validated();
        $user = $request->user();

        $postData['generation_user_year_id'] = $user->currentGenrationUserYear->id;
        $postData['user_id'] = $user->id;

        if(!empty($postData['tab_id'])){

        $tab = $this->tabsRepository->checkUserTab($user,$postData['tab_id']);
        if(empty($tab) || !$tab->is_public)
        return ResponseHelper::authorizationFail();

        } else {

         $userSubject = $userSubjectsRepositroy->checkUserSubject($user->id,$postData['subject_id']);
        if(empty($userSubject))
        return ResponseHelper::authorizationFail();

        }

        $post = $this->postRepository->create($postData);
        if(empty($post))
        return ResponseHelper::operationFail();

         return ResponseHelper::insert($post);

    }


    public function update(UpdatePostRequest $request)
    {
        $user = $request->user();

        $data = $request->validated();
        $postId = $data['post_id'];
        unset($data['post_id']);

        $post = $this->postRepository->find(['id' => $postId]);
        $this->authorize('update',$post);

        $updatePostResult = $this->postRepository->update($postId,$data);
        if(empty($updatePostResult))
        return ResponseHelper::operationFail();

        return ResponseHelper::update($post->refresh());
    }

    public function get(GetPostsRequest $request)
    {
        $data = $request->validated();
        $posts = $this->postRepository->get($data);
        return ResponseHelper::select($posts);
    }

    public function find(FindPostRequest $request)
    {
        $data = $request->validated();
        $user = $request->user();
        $postId = $data['post_id'];
        unset($data['post_id']);
        $data['id'] = $postId;

        $post = $this->postRepository->find($data);
        if(empty($post))
        return ResponseHelper::DataNotFound();

        $tab = $this->tabsRepository->checkUserTab($user,$post->tab_id);
        if(empty($tab))
        return ResponseHelper::authorizationFail();

        return ResponseHelper::select($post);
    }

    /*public function paginate(PaginatePostsRequest $request)
    {
        $data = $request->validated();
        $posts = $this->postRepository->paginate($data);
        return ResponseHelper::select($posts);
    }*/


    public function getTabPosts(GetTabPostRequest $request)
    {
        $data = $request->validated();
        $user = $request->user();

        $tab = $this->tabsRepository->checkUserTab($user,$data['tab_id']);
        if(empty($tab))
        return ResponseHelper::authorizationFail();

        $filter['tab_id'] = $data['tab_id'];
        return ResponseHelper::select($this->postRepository->paginate($filter));
    }

    public function delete(DeletePostRequest $request)
    {
        $data = $request->validated();

        $post = $this->postRepository->find(['id' => $data['post_id']]);

        if(empty($post))
        return ResponseHelper::DataNotFound();

        $this->authorize('delete',$post);

       $deletedPost = $this->postRepository->delete($data['post_id']);

       if(empty($deletedPost))
       return ResponseHelper::operationFail();

       return ResponseHelper::delete();
    }

    public function uploadMedia(UploadMediaRequest $request)
    {
        $file = $request->file('file');
        $uuid = Str::uuid();
        $fileName = "file_{$uuid}.{$file->extension()}";
        $path = '/media/';
        try {
            Storage::disk('pub')
            ->put($path.$fileName, fopen($file, 'r+'),'public');
            return ResponseHelper::select($path.$fileName);
        }catch (\Exception $exception)
        {
            return ResponseHelper::operationFail();
        }

    }


    public function getGeneralQuestion(Request $request)
    {
        $user = $request->user();

        return ResponseHelper::
        select($this->postRepository->getGeneralQuestions($user));
    }


    public function generalQuestionFilters(Request $request)
    {
        $user = $request->user();

        return ResponseHelper::select($this->postRepository
        ->getGeneralQuestionFilters($user));
    }

    public function getUserYearSemesterSubject(GetUserYearSemesterSubjectRequest $request)
    {
        $user = $request->user();
        $data = $request->validated();

        return ResponseHelper::select($this->postRepository
        ->getUserYearSemesterSubject($user,$data['year_id'],$data['semester_id']));
    }


}
