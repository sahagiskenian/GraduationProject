<?php

namespace App\Http\Controllers\API\University_Common;
use App\Repositories\Interfaces\University_Common\UniversityInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class UniversityController extends Controller
{
    //

    private $universityRepository;

    public function __construct(UniversityInterface $universityRepository)
    {
        $this->universityRepository = $universityRepository;
    }


    public function index(){

   return $this->universityRepository->GetUniversities();
    }
}
