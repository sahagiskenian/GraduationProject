<?php

namespace App\Http\Controllers\API\Feedback;

use App\Http\Controllers\Controller;
use App\Models\Feedback\Advertising;
use Illuminate\Http\Request;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class AdvertisingController extends Controller
{
    //

    public function get(){

       $advertisings= Advertising::all();

       return ResponseHelper::select($advertisings);
    }
}
