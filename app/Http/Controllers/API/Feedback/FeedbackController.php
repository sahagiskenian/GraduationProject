<?php

namespace App\Http\Controllers\API\Feedback;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\Feedback\FeedbackInterface;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    //
    private $feedbackRepository;

    public function __construct(FeedbackInterface $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
    }


    public function AddFeedback(Request $request){

        return $this->feedbackRepository->AddFeedback($request);

    }
}
