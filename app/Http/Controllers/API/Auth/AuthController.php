<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use App\Models\User\UserToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
class AuthController extends Controller
{
    public function register (Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'college_id' => 'required',
            'university_id' => 'required',
            'mobile' => 'required',
            'collage_number' => 'required',
            'user_token' => 'required|string',
            'device_id' => 'required|string',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $request['password']=Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $user = User::create($request->toArray());
        $user->user_type_id=5;
        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $user->save();
        $user->token=$token;

        UserToken::updateOrCreate(['device_id' => $request->get('device_id')],[
            'token' => $request->get('user_token'),
            'user_id' => $user->id,
        ]);

        $response = ['user'=> $user];

        return response($response, 200);
    }

    public function login (Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
            'user_token' => 'required|string',
            'device_id' => 'required|string',
        ]);

        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $user = User::with(['college','university','galleryUsers.gallery',
        'posts'=> function ($query) {
            $query->orderBy('created_at','desc')->get();
        }
        ,'searchs' => function ($query) {
            $query->orderBy('created_at','desc')->get();
        }])->where('email', $request->email)->first();

        if ($user) {
            if($user->verified_at){
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $user->token=$token;

                UserToken::updateOrCreate(['device_id' => $request->get('device_id')],[
                    'token' => $request->get('user_token'),
                    'user_id' => $user->id,
                ]);

                $response = ['user'=> $user ];
                return response($response, 200);
            } else {
                $response = ["message" => "Password mismatch"];
                return response($response, 422);
            }
        } else {
            $response = ["message" =>'User Still Not Accepted'];
            return response($response, 422);
        }
    }
    else {
        $response = ["message" =>'User does not exist'];
        return response($response, 422);
    }
    }
    public function logout (Request $request) {
        $user = $request->user();
        if($request->has('device_id'))
        {
            UserToken::where('user_id' , $user->id)
                        ->where('device_id',$request->get('device_id'))
                        ->delete();
        }
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }

}
