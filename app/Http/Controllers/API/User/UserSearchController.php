<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\Users\UserSearchInterface;
use Illuminate\Http\Request;

class UserSearchController extends Controller
{
    //
    protected $searchsRepository;

    public function __construct(UserSearchInterface $searchsRepository)
    {
        $this->searchsRepository = $searchsRepository;

    }


    public function get(Request $request){

        return $this->searchsRepository->getUserSearchs($request->user());
    }
    public function post(Request $request){

        return $this->searchsRepository->postUserSearchs($request->search_text,$request->user());
    }




    public function remove(Request $request){

        return $this->searchsRepository->ClearSearchs($request->user());
    }

}
