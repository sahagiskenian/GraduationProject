<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\Save\CreateSaveRequest;
use App\Repositories\Interfaces\Users\SaveInterface;
use Illuminate\Http\Request;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class SaveController extends Controller
{
    //
    protected $SaveInterface;

    public function __construct(SaveInterface $SaveInterface)
    {
        $this->SaveInterface = $SaveInterface;

    }



    public function post(CreateSaveRequest $request){
        $postData = $request->validated();





         $save = $this->SaveInterface->CreateSave($postData);
         if(empty($save))
         return ResponseHelper::operationFail();

         return ResponseHelper::insert($save);

    }

    public function delete($id){


        $post = $this->SaveInterface->FindSave($id);

        if(empty($post))
        return ResponseHelper::DataNotFound();



       $deleted = $this->SaveInterface->RemoveSave($id);

       if(empty($deleted))
       return ResponseHelper::operationFail();

       return ResponseHelper::delete();

    }
}
