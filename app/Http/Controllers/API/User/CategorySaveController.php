<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CategorySave\CreateCategorySaveRequest;
use App\Repositories\Interfaces\Users\CategorySaveInterface;
use Illuminate\Http\Request;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class CategorySaveController extends Controller
{
    //
    protected $categorySaveInterface;

    public function __construct(CategorySaveInterface $categorySaveInterface)
    {
        $this->categorySaveInterface = $categorySaveInterface;

    }

    public function get(Request $request){
     $categories=   $this->categorySaveInterface->GetUserCategorySave($request->user()->id);


    return ResponseHelper::select($categories);
    }

    public function post(CreateCategorySaveRequest $request){
        $postData = $request->validated();
        $user = $request->user()->id;


        $postData['user_id'] = $user;




         $category = $this->categorySaveInterface->CreateCategorySave($postData);
         if(empty($category))
         return ResponseHelper::operationFail();

         return ResponseHelper::insert($category);

    }

    public function delete($id){


        $post = $this->categorySaveInterface->FindCategorySave($id);

        if(empty($post))
        return ResponseHelper::DataNotFound();



       $deletedCategory = $this->categorySaveInterface->RemoveCategorySave($id);

       if(empty($deletedCategory))
       return ResponseHelper::operationFail();

       return ResponseHelper::delete();

    }
}
