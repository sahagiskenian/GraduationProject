<?php

namespace App\Http\Controllers\API\College_Common;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\College_Common\GalleryInterface;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    //
    private $galleryRepository;

    public function __construct(GalleryInterface $galleryRepository)
    {
        $this->galleryRepository = $galleryRepository;
    }

    public function GetCollegeGallery($id){
        return $this->galleryRepository->GetCollegeGallery($id);
    }

   public function GetMyGallery(){

    return $this->galleryRepository->GetMyGallery();
   }

   public function RemoveFromMyGallery($id){

    return $this->galleryRepository->RemoveFromMyGallery($id);
   }

   public function GetGalleryDetails($id){

    return $this->galleryRepository->GetGalleryDetails($id);
   }

   public function AddStudentToGallery(Request $request){

    return $this->galleryRepository->AddStudentToGallery($request);
   }

   public function EditGallery(Request $request){

    return $this->galleryRepository->EditGallery($request);
   }

   public function RemoveStudentFromGallery($gallery_id,$user_id){

    return $this->galleryRepository->RemoveStudentFromGallery($gallery_id,$user_id);
   }
}
