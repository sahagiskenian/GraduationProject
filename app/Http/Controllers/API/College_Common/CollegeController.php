<?php

namespace App\Http\Controllers\API\College_Common;

use App\Repositories\Interfaces\College_Common\CollegeInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CollegeController extends Controller
{
    private $collegeRepository;

    public function __construct(CollegeInterface $collegeRepository)
    {
        $this->collegeRepository = $collegeRepository;
    }

    public function GetUniversityColleges($id) {
    return    $this->collegeRepository->GetUniversityColleges($id);
    }

    public function GetCollegeDepartments($id) {
        return    $this->collegeRepository->GetCollegeDeparments($id);
        }

        public function GetCollegeDetails($id) {
            return    $this->collegeRepository->GetCollegeDetails($id);
            }
}
