<?php

namespace App\Http\Controllers\API\College_Common;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\College_Common\CommonQuestionInterface;
use Illuminate\Http\Request;

class CommonQuestionController extends Controller
{
    //
    private $commonQuestionInterface;

    public function __construct(CommonQuestionInterface $commonQuestionInterface)
    {
        $this->commonQuestionInterface = $commonQuestionInterface;
    }

    public function GetAllCommonQuestions(){

        return $this->commonQuestionInterface->GetAllCommonQuestions();
    }

    public function GetCollegeCommonQuestions($id){

        return $this->commonQuestionInterface->GetCollegeCommonQuestions($id);
    }
}
