<?php

namespace App\Http\Controllers\API\College_Common;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\College_Common\DepartmentInterface;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    //
    private $departmentRepository;

    public function __construct(DepartmentInterface $departmentRepository)
    {
        $this->departmentRepository = $departmentRepository;
    }

    public function GetDepartmentSubjects($id){


return $this->departmentRepository->GetDepartmentSubjects($id);

    }
}
