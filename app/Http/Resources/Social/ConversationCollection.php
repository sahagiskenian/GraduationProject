<?php

namespace App\Http\Resources\Social;

use App\Models\Social\Conversation;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ConversationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($conversation){

             return new ConversationResource($conversation);
        });

    }
}
