<?php

namespace App\Http\Resources\Social;

use App\Enums\ConversationTypes;
use Illuminate\Http\Resources\Json\JsonResource;

class ConversationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'conversation_header' => $this->when($this->type == ConversationTypes::single,function() use ($request)
            {
                $user = ($request->user()->id != $this->sender->id) ? $this->sender : $this->receiver;
                return [
                    'title' => $user->first_name .' '. $user->last_name,
                    'img' => $user->profile_pic,
                ];
            },function()
            {
                return [
                    'title' => $this->convesationGroupHeader->title,
                    'img' => $this->convesationGroupHeader->img,
                ];
            }),
            'last_message' => $this->lastMessage,
            'un_read_message_count' => $this->un_read_conversation_message_receiver_count,
            'created_at' => $this->created_at,
        ];
    }
}
