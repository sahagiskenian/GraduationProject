<?php



namespace App\Models\Social;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class ConversationMessageReciever extends BaseModel
{
    use HasFactory,SoftDeletes;

    protected $table = 'conversation_message_receivers';

    protected $fillable = ['read_at','conversation_message_id','receiver_id'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = ['read_at'];

    protected $casts = [
        'conversation_message_id' => 'integer',
        'receiver_id' => 'integer',
    ];

    public function receiver()
    {
        return $this->belongsTo(User::class,'receiver_id');
    }

    public function conversationMessage()
    {
        return $this->belongsTo(ConversationMessage::class);
    }

}
