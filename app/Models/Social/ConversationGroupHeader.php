<?php

namespace App\Models\Social;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kouja\ProjectAssistant\Bases\BaseModel;

class ConversationGroupHeader extends BaseModel
{
    use HasFactory;

    protected $table = 'conversation_group_headers';

    protected $fillable = ['title','img','conversation_id'];

    protected $cast = [
        'conversation_id' => 'integer',
    ];

    public function converastion()
    {
        return $this->belongsTo(Conversation::class);
    }
}
