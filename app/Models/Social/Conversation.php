<?php



namespace App\Models\Social;

use App\Models\College_Common\Generation;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Conversation extends BaseModel
{
    use HasFactory,SoftDeletes;

    protected $table = 'conversations';

    protected $fillable = ['description','sender_id','receiver_id','generation_id','type'];

    protected $hidden = ['deleted_at'];

    protected $dates = [];

    protected $casts = [
        'sender_id' => 'integer',
        'receiver_id' => 'integer',
        'generation_id' => 'integer',
    ];

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class,'receiver_id');
    }

    public function generation()
    {
        return $this->belongsTo(Generation::class);
    }

    public function conversationGroupUsers()
    {
        return $this->hasMany(ConversationGroupUser::class);
    }

    public function conversationMessage()
    {
        return $this->hasMany(ConversationMessage::class)
        ->latest();
    }

    public function unReadConversationMessageReceiver()
    {
        return $this->hasManyThrough(ConversationMessageReciever::class,ConversationMessage::class,'conversation_id','conversation_message_id')
        ->where('receiver_id',Auth::id())
        ->whereNull('conversation_message_receivers.read_at');
    }

    public function lastMessage()
    {
        return $this->hasOne(ConversationMessage::class)
        ->latest();
    }

    public function convesationGroupHeader()
    {
        return $this->hasOne(ConversationGroupHeader::class)
        ->select('title','img','conversation_id');
    }



}
