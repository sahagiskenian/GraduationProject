<?php



namespace App\Models\Social;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class React extends BaseModel
{
    use HasFactory,SoftDeletes;

    protected $table = 'reacts';

    protected $fillable = ['type','generation_user_year_id','post_id','comment_id','user_id'];

    protected $hidden = [];

    protected $dates = [];

    protected $casts = [
        'generation_user_year_id' =>'integer',
        'post_id' =>'integer',
        'comment_id' =>'integer',
        'user_id' => 'integer',
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
