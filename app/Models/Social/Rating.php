<?php



namespace App\Models\Social;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Rating extends BaseModel
{
    use HasFactory,SoftDeletes;

    protected $table = 'ratings';

    protected $fillable = ['rate','post_id','generation_user_year_id','comment_id','user_id'];

    protected $hidden = [];

    protected $dates = [];

    protected $casts = [
        'post_id' => 'integer',
        'generation_user_year_id' => 'integer',
        'comment_id' => 'integer',
        'user_id' => 'integer',
        'rate' => 'integer',
    ];

}
