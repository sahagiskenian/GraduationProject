<?php

namespace App\Models\Social;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class ConversationGroupUser extends BaseModel
{
    use HasFactory,SoftDeletes;

    protected $table = 'conversation_group_users';

    protected $fillable = ['user_id','conversation_id','type'];

    protected $casts = [
        'user_id' => 'integer',
        'conversation_id' => 'integer'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }
}
