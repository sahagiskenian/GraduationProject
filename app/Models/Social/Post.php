<?php



namespace App\Models\Social;

use App\Models\Subject\Subject;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;
class Post extends BaseModel
{
    use HasFactory,SoftDeletes;

    protected $table = 'posts';

    protected $fillable = ['title','description','tab_id','user_id','generation_user_year_id','subject_id'];

    protected $hidden = [];

    protected $dates = [];

    protected $casts = [
        'tab_id' => 'integer',
        'user_id' => 'integer',
        'generation_user_year_id' => 'integer',
        'subject_id' => 'integer'
    ];


    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function rates()
    {
        return $this->hasMany(Rating::class);
    }

    public function reacts()
    {
        return $this->hasMany(React::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tab()
    {
        return $this->belongsTo(Tabs::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function scopeGeneralQuestion()
    {
        return $this->whereNotNull('posts.subject_id');
    }

}
