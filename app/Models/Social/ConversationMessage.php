<?php



namespace App\Models\Social;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Kouja\ProjectAssistant\Bases\BaseModel;

class ConversationMessage extends BaseModel
{
    use HasFactory,SoftDeletes;

    protected $table = 'conversation_messages';

    protected $fillable = ['description','conversation_id','sender_id'];

    protected $hidden = [];

    protected $dates = ['read_at'];

    protected $casts = [
        'conversation_id' => 'integer',
        'sender_id' => 'integer',
    ];

    protected $appends = ['is_sender'];

    public function getIsSenderAttribute()
    {
        return $this->attributes['sender_id'] == Auth::id();
    }

    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }

    public function conversationMessageReceivers()
    {
        return $this->hasMany(ConversationMessageReciever::class);
    }

}
