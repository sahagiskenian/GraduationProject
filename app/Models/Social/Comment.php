<?php



namespace App\Models\Social;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Comment extends BaseModel
{
    use HasFactory,SoftDeletes;

    protected $table = 'comments';

    protected $fillable = ['description','post_id','user_id','comment_id','generation_user_year_id'];

    protected $hidden = ['deleted_at'];

    protected $dates = [];

    protected $casts = [
        'post_id' => 'integer',
        'user_id' => 'integer',
        'comment_id' => 'integer',
        'generation_user_year_id' => 'integer',
    ];

    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function reacts()
    {
        return $this->hasMany(React::class);
    }

    public function rates()
    {
        return $this->hasMany(Rating::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
