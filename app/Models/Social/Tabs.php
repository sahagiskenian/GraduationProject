<?php



namespace App\Models\Social;

use App\Models\College_Common\College;
use App\Models\College_Common\GenerationYear;
use App\Models\University_Common\Specialization;
use App\Models\University_Common\University;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Tabs extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'tabs';

    protected $fillable = ['name','type','is_public','university_id','collage_id','generation_year_id','specialization_id'];

    protected $hidden = [];

    protected $dates = [];

    protected $casts = [
        'university_id' => 'integer',
        'collage_id' => 'integer',
        'generation_year_id' => 'integer',
        'specialization_id' => 'integer',
        'is_public' => 'boolean',
    ];

    public function university(){
        return $this->belongsTo(University::class);
    }

    public function college(){
        return $this->belongsTo(College::class);
    }

    public function specialization(){
        return $this->belongsTo(Specialization::class);
    }


    public function generation_year(){
        return $this->belongsTo(GenerationYear::class);
    }

}
