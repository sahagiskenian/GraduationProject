<?php



namespace App\Models\Subject;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Subscription extends BaseModel
{
    use HasFactory;

    protected $table = 'subscriptions';

    protected $fillable = ['subject_id','generation_user_year_id'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [
        'generation_user_year_id' => 'integer',
        'subject_id' => 'integer',
    ];

}
