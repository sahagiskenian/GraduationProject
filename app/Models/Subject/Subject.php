<?php



namespace App\Models\Subject;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Subject extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'subjects';

    protected $fillable = ['name','description','year_id','semester_id','department_id'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [
        'year_id' => 'integer',
        'semester_id' => 'integer',
        'department_id' => 'integer',
    ];


    public function department()
    {
        return $this->belongsTo('App\Models\College_Common\Department');
    }
    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function userSubjects()
    {
        return $this->hasMany(UserSubjects::class);
    }

    public function firstCommonSubject()
    {
        return $this->hasMany(CommonSubject::class,'first_subject_id');
    }

    public function secondCommonSubject()
    {
        return $this->hasMany(CommonSubject::class,'second_subject_id');
    }
}
