<?php



namespace App\Models\Subject;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class CommonSubject extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'common_subjects';

    protected $fillable = ['first_subject_id','second_subject_id'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [
        'first_subject_id' => 'integer',
        'second_subject_id' => 'integer',
    ];


    public function FirstSubject()
    {
        return $this->belongsTo(Subject::class,'first_subject_id');
    }

    public function SecondSubject()
    {
        return $this->belongsTo(Subject::class,'second_subject_id');
    }

}
