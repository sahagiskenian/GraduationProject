<?php



namespace App\Models\Subject;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class File extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'files';

    protected $fillable = ['name','user_id','subject_id','generation_year_id','url'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [
        'user_id' => 'integer',
        'subject_id' => 'integer',
        'generation_year_id' => 'integer',
    ];

}
