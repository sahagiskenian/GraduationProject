<?php



namespace App\Models\Subject;

use App\Models\University_Common\Semester;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Kouja\ProjectAssistant\Bases\BaseModel;

class UserSubjects extends BaseModel
{
    use HasFactory;

    protected $table = 'user_subjects';

    protected $fillable = ['semester_id','user_id','subject_id','generation_user_year_id'];

    protected $hidden = [];

    protected $dates = [];

    protected $casts = [
        'semester_id' => 'integer',
        'user_id' => 'integer',
        'subject_id' => 'integer',
        'generation_user_year_id' => 'integer',
    ];


    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }

    public function generationUserYear()
    {
        return $this->belongsTo(generationUserYear::class);
    }

}
