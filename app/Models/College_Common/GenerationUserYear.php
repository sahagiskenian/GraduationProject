<?php

namespace App\Models\College_Common;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GenerationUserYear extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function generationyear()
    {
        return $this->belongsTo(GenerationYear::class,'generation_year_id');
    }
}
