<?php

namespace App\Models\College_Common;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GalleryUser extends Model
{
    use HasFactory;
 protected $fillable=[
     'gallery_id',
     'user_id'
 ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }
}
