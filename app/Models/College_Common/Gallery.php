<?php

namespace App\Models\College_Common;

use App\Models\University_Common\UniversityCalender;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;
    public function department()
    {
        return $this->belongsTo('App\Models\College_Common\Department');
    }
    public function university_calender()
    {
        return $this->belongsTo(UniversityCalender::class);
    }
    public function galleryusers()
    {
        return $this->hasMany(GalleryUser::class);
    }
}
