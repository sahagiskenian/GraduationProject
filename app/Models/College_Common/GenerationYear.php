<?php

namespace App\Models\College_Common;

use App\Models\University_Common\UniversityCalender;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GenerationYear extends Model
{
    use HasFactory;
    protected $fillable=[
        "generation_id",
        "year_id",
        "university_calender_id",
        "department_id"
    ];


    public function generationuseryears()
    {
        return $this->hasMany('App\Models\College_Common\GenerationUserYear');
    }
    public function year()
    {
        return $this->belongsTo('App\Models\College_Common\Year');
    }
    public function department()
    {
        return $this->belongsTo('App\Models\College_Common\Department');
    }
    public function generation()
    {
        return $this->belongsTo('App\Models\College_Common\Generation');
    }
    public function university_calender()
    {
        return $this->belongsTo(UniversityCalender::class);
    }
}
