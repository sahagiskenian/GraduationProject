<?php

namespace App\Models\College_Common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Subject\Subject;

class Year extends Model
{
    use HasFactory;

    public function college()
    {
        return $this->belongsTo('App\Models\Common_College\College');
    }
    public function generationyears()
    {
        return $this->hasMany('App\Models\College_Common\GenerationYear');
    }

    public function subjects()
    {
        return $this->hasMany(Subject::class);
    }
}
