<?php

namespace App\Models\College_Common;

use App\Models\Subject\File;
use App\Models\Subject\Subject;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class College extends Model
{
    use HasFactory;


    public function galleries()
    {
        return $this->hasManyThrough(Gallery::class, Department::class);
    }
    public function subjects()
    {
        return $this->hasManyThrough(Subject::class, Department::class);
    }
    public function files()
    {
        return $this->hasManyDeep(File::class, [Department::class,Subject::class]);
    }
    public function university()
    {
        return $this->belongsTo('App\Models\University_Common\University');
    }

    public function specialization()
    {
        return $this->belongsTo('App\Models\University_Common\Specialization');
    }
    public function common_questions()
    {
        return $this->hasMany('App\Models\College_Common\CommonQuestion');
    }
    public function departments()
    {
        return $this->hasMany(Department::class);
    }
    public function years()
    {
        return $this->hasMany(Year::class);
    }
    public function users()
    {
        return $this->hasMany(User::class);
    }
    public function generations()
    {
        return $this->hasMany(Generation::class);
    }
}
