<?php

namespace App\Models\College_Common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommonQuestion extends Model
{
    use HasFactory;


    public function college()
    {
        return $this->belongsTo(College::class);
    }
}
