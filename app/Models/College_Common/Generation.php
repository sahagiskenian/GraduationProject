<?php

namespace App\Models\College_Common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Generation extends Model
{
    use HasFactory;
protected $fillable=[
    "name",
    "student_num",
    "entry_date",
    "graduation_date",
    "description",
    "college_id"
];


    public function college()
    {
        return $this->belongsTo(College::class);
    }
    public function generationyears()
    {
        return $this->hasMany(GenerationYear::class);
    }
}
