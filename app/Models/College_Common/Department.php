<?php

namespace App\Models\College_Common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;


    public function college()
    {
        return $this->belongsTo('App\Models\College_Common\College');
    }
    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }
    public function generationyears()
    {
        return $this->hasMany('App\Models\College_Common\GenerationYear');
    }
    public function subjects()
    {
        return $this->hasMany('App\Models\Subject\Subject');
    }
}
