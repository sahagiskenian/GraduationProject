<?php

namespace App\Models\University_Common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    use HasFactory;


    public function calenders()
    {
        return $this->hasMany('App\Models\University_Common\UniversityCalender');
    }

    public function semesters()
    {
        return $this->hasMany('App\Models\University_Common\Semester');
    }
    public function colleges()
    {
        return $this->hasMany('App\Models\College_Common\College');
    }
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
