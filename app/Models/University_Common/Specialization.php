<?php

namespace App\Models\University_Common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    use HasFactory;

    public function specializationtype()
    {
        return $this->belongsTo('App\Models\SpecializationType');
    }
    public function specializations()
    {
        return $this->hasMany('App\Models\College_Common\College');
    }
}
