<?php

namespace App\Models\University_Common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecializationType extends Model
{
    use HasFactory;

    public function specializations()
    {
        return $this->hasMany('App\Models\Specialization');
    }
}
