<?php

namespace App\Models\University_Common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    use HasFactory;




    public function university()
    {
        return $this->belongsTo('App\Models\University');
    }
}
