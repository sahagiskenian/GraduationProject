<?php

namespace App\Models\University_Common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UniversityCalender extends Model
{
    use HasFactory;


    public function university()
    {
        return $this->belongsTo('App\Models\University');
    }
    public function galleries()
    {
        return $this->hasMany('App\Models\College_Common\Gallery');
    }
    public function generationyears()
    {
        return $this->hasMany('App\Models\College_Common\GenerationYear');
    }
}
