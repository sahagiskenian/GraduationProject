<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSearch extends Model
{
    use HasFactory;

    protected $fillable=[
'user_id',
'search_text'
    ];


    public function user(){

        return $this->belongsTo(User::class);
    }
}
