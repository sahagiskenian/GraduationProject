<?php



namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class CategorySave extends BaseModel
{
    use HasFactory,SoftDeletes;

    protected $table = 'category_saves';

    protected $fillable = ['name','user_id'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [
        'user_id' => 'integer',
    ];


    public function user(){

        return $this->belongsTo(User::class);
    }

    public function saves(){

        return $this->hasMany(Save::class);
    }

}
