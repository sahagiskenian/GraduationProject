<?php

namespace App\Models\User;

use App\Models\College_Common\GalleryUser;
use App\Models\College_Common\GenerationUserYear;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Models\College_Common\College;
use App\Models\College_Common\Generation;
use App\Models\College_Common\GenerationYear;
use App\Models\Social\Post;
use App\Models\University_Common\University;

class User extends Authenticatable
{
    use HasFactory, Notifiable,SoftDeletes,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','middle_name' ,'last_name','college_id','profile_pic','mobile','facebook_url','linked_in_url',
        'email', 'password','user_type_id','collage_number','unviersity_id'
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    protected $dates = ['email_verified_at','verified_at','suspended_at'];

    protected $casts = [
        'user_type_id' => 'integer',
        'college_id' => 'integer',
        'collage_number' => 'integer',
    ];



    public function galleryUsers()
    {
        return $this->hasMany(GalleryUser::class);
    }
    public function generationUserYears()
    {
        return $this->hasMany(GenerationUserYear::class);
    }

    public function generationYears()
    {
        return $this->hasMany(GenerationUserYear::class);
    }

    public function currentGenrationUserYear()
    {
        return $this->hasOne(GenerationUserYear::class)
        ->latest();
    }


    public function college()
    {
        return $this->belongsTo(College::class,'college_id');
    }

    public function university()
    {
        return $this->belongsTo(University::class);
    }
    public function searchs()
    {
        return $this->hasMany(UserSearch::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function userTokens()
    {
        return $this->hasMany(UserToken::class);
    }
}
