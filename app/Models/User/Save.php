<?php



namespace App\Models\User;

use App\Models\Social\Post;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Save extends BaseModel
{
    use HasFactory,SoftDeletes;

    protected $table = 'saves';

    protected $fillable = ['post_id','category_save_id'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [
        'post_id' => 'integer',
        'category_save_id' => 'integer',
    ];


    public function categorysave(){
        return $this->belongsTo(CategorySave::class);
    }

    public function post(){
        return $this->belongsTo(Post::class);
    }

}
