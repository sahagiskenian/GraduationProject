<?php



namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Kouja\ProjectAssistant\Bases\BaseModel;

class UserToken extends BaseModel
{
    use HasFactory;

    protected $table = 'user_tokens';

    protected $fillable = ['token','user_id','device_id'];

    protected $hidden = [];

    protected $dates = [];

    protected $casts = [
        'user_id' => 'integer',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
