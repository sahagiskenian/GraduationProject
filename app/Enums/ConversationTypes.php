<?php

namespace App\Enums;

use BenSampo\Enum\Enum;


final class ConversationTypes extends Enum
{
    const single =   'single';
    const group =   'group';
}
