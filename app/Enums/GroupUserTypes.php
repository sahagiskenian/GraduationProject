<?php

namespace App\Enums;

use BenSampo\Enum\Enum;


final class GroupUserTypes extends Enum
{
    const user =   'user';
    const admin =   'admin';
}
