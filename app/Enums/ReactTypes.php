<?php

namespace App\Enums;

use BenSampo\Enum\Enum;


final class ReactTypes extends Enum
{
    const sad =   'sad';
    const love =   'love';
    const like =   'like';
    const anger =   'anger';
    const laugh =   'laugh';
}
