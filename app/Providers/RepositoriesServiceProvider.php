<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Implementations\Users\UserTypeRepository;
use App\Repositories\Interfaces\Users\UserTypeInterface;
use App\Repositories\Implementations\College_Common\CollegeRepository;
use App\Repositories\Implementations\College_Common\CommonQuestionRepository;
use App\Repositories\Implementations\College_Common\DepartmentRepository;
use App\Repositories\Implementations\College_Common\GalleryRepository;
use App\Repositories\Implementations\Feedback\FeedbackRepository;
use App\Repositories\Implementations\Social\CommentRepository;
use App\Repositories\Implementations\Social\ConversationGroupUserRepository;
use App\Repositories\Implementations\Social\ConversationMessageRepository;
use App\Repositories\Implementations\Social\ConversationRepository;
use App\Repositories\Implementations\Social\PostRepository;
use App\Repositories\Implementations\Social\RateRepository;
use App\Repositories\Implementations\Social\ReactRepository;
use App\Repositories\Implementations\Social\TabRepository;
use App\Repositories\Implementations\Social\TabsRepository;
use App\Repositories\Implementations\Subject\FileRepository;
use App\Repositories\Implementations\Subject\SubjectRepository;
use App\Repositories\Implementations\Subject\UserSubjectsRepository;
use App\Repositories\Interfaces\College_Common\CollegeInterface;
use App\Repositories\Implementations\University_Common\UniversityRepository;
use App\Repositories\Implementations\Users\CategorySaveRepository;
use App\Repositories\Implementations\Users\SaveRepository;
use App\Repositories\Implementations\Users\UserSearchRepository;
use App\Repositories\Interfaces\College_Common\CommonQuestionInterface;
use App\Repositories\Interfaces\College_Common\DepartmentInterface;
use App\Repositories\Interfaces\College_Common\GalleryInterface;
use App\Repositories\Interfaces\Feedback\FeedbackInterface;
use App\Repositories\Interfaces\Social\CommentInterface;
use App\Repositories\Interfaces\Social\ConversationGroupUserInterface;
use App\Repositories\Interfaces\Social\ConversationInterface;
use App\Repositories\Interfaces\Social\ConversationMessageInterface;
use App\Repositories\Interfaces\Social\PostInterface;
use App\Repositories\Interfaces\Social\RateInterface;
use App\Repositories\Interfaces\Social\ReactInterface;
use App\Repositories\Interfaces\Social\TabInterface;
use App\Repositories\Interfaces\Social\TabsInterface;
use App\Repositories\Interfaces\Subject\FileInterface;
use App\Repositories\Interfaces\Subject\SubjectInterface;
use App\Repositories\Interfaces\Subject\UserSubjectsInterface;
use App\Repositories\Interfaces\University_Common\UniversityInterface;
use App\Repositories\Interfaces\Users\CategorySaveInterface;
use App\Repositories\Interfaces\Users\SaveInterface;
use App\Repositories\Interfaces\Users\UserSearchInterface;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(UserTypeInterface::class, UserTypeRepository::class);
        $this->app->bind(CollegeInterface::class, CollegeRepository::class);
        $this->app->bind(UniversityInterface::class, UniversityRepository::class);
        $this->app->bind(SubjectInterface::class, SubjectRepository::class);
        $this->app->bind(DepartmentInterface::class, DepartmentRepository::class);
        $this->app->bind(FeedbackInterface::class, FeedbackRepository::class);
        $this->app->bind(CommonQuestionInterface::class, CommonQuestionRepository::class);
        $this->app->bind(GalleryInterface::class, GalleryRepository::class);
        $this->app->bind(PostInterface::class,PostRepository::class);
        $this->app->bind(TabInterface::class,TabRepository::class);
        $this->app->bind(FileInterface::class, FileRepository::class);
        $this->app->bind(TabsInterface::class, TabsRepository::class);
        $this->app->bind(CategorySaveInterface::class,CategorySaveRepository::class);
        $this->app->bind(SaveInterface::class,SaveRepository::class);
        $this->app->bind(UserSearchInterface::class,UserSearchRepository::class);
        $this->app->bind(CommentInterface::class,CommentRepository::class);
        $this->app->bind(RateInterface::class,RateRepository::class);
        $this->app->bind(ReactInterface::class,ReactRepository::class);
        $this->app->bind(ConversationInterface::class,ConversationRepository::class);
        $this->app->bind(ConversationMessageInterface::class,ConversationMessageRepository::class);
        $this->app->bind(ConversationGroupUserInterface::class,ConversationGroupUserRepository::class);
        $this->app->bind(UserSubjectsInterface::class,UserSubjectsRepository::class);



    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
