<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom([
                database_path('migrations/University_Common'),
                database_path('migrations/College_Common'),
                database_path('migrations/Social'),
                database_path('migrations/Subject'),
                database_path('migrations/User'),
            ]);



    }
}
