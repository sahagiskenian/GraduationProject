<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/admin';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));

                Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api/User/api.php'));

                Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api/Subject/api.php'));

                Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api/Social/api.php'));

                Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api/Feedback/api.php'));

                Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api/University_Common/api.php'));

                Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api/College_Common/api.php'));
        });
        $this->mapAuthRoutes();
        $this->mapAdminWebRoutes();
        $this->mapCollegeAdminWebRoutes();

        $this->mapUserWebRoutes();




    }

    protected function mapAdminWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web/admin.php'));
    }
    protected function mapAuthRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web/authroutes.php'));
    }
  /*  protected function mapSuperAdminWebRoutes()
    {
        Route::middleware(['web','super_admin'])
            ->namespace($this->namespace)
            ->group(base_path('routes/web/admin.php'));
    }
    protected function mapAdminWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web/admin.php'));
    }
    protected function mapAdminWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web/admin.php'));
    }*/
    protected function mapCollegeAdminWebRoutes()
    {


        Route::middleware(['web','college_admin'])
            ->namespace($this->namespace)
            ->group(base_path('routes/web/Dashboard/CollegeAdmin/Subject/subject.php'));
      Route::middleware(['web','college_admin'])
          ->namespace($this->namespace)
          ->group(base_path('routes/web/Dashboard/CollegeAdmin/RelatedSubjects/relatedsubjects.php'));
          Route::middleware(['web','college_admin'])
          ->namespace($this->namespace)
          ->group(base_path('routes/web/Dashboard/CollegeAdmin/College_Common/generations.php'));
          Route::middleware(['web','college_admin'])
          ->namespace($this->namespace)
          ->group(base_path('routes/web/Dashboard/CollegeAdmin/College_Common/gallery.php'));
          Route::middleware(['web','college_admin'])
          ->namespace($this->namespace)
          ->group(base_path('routes/web/Dashboard/CollegeAdmin/Subject/file.php'));
    }

    protected function mapUserWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web/user.php'));
    }



    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
