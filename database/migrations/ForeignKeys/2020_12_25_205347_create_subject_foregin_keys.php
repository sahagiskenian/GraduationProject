<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectForeginKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects', function (Blueprint $table) {
            $table->foreignId('year_id')->nullable()->constrained();
            $table->foreignId('semester_id')->nullable()->constrained();
            $table->foreignId('department_id')->nullable()->constrained();
        });

        Schema::table('user_subjects', function (Blueprint $table) {
            $table->foreignId('subject_id')->constrained();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('semester_id')->constrained();
            $table->foreignId('generation_user_year_id')->constrained();
        });

        Schema::table('common_subjects', function (Blueprint $table) {
            $table->foreignId('first_subject_id')->nullable()->constrained('subjects','id');
            $table->foreignId('second_subject_id')->nullable()->constrained('subjects','id');
        });

        Schema::table('files', function (Blueprint $table) {
            $table->foreignId('user_id')->nullable()->constrained();
            $table->foreignId('subject_id')->nullable()->constrained();
            $table->foreignId('generation_year_id')->nullable()->constrained();
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->foreignId('subject_id')->nullable()->constrained();
            $table->foreignId('generation_user_year_id')->nullable()->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
