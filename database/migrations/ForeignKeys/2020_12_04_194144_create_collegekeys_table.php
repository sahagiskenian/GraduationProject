<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollegekeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('colleges', function (Blueprint $table) {
            $table->foreignId('university_id')->nullable()->constrained();
            $table->foreignId('specialization_id')->nullable()->constrained();
        });
        Schema::table('common_questions', function (Blueprint $table) {
            $table->foreignId('college_id')->nullable()->constrained();

        });
        Schema::table('departments', function (Blueprint $table) {
            $table->foreignId('college_id')->nullable()->constrained();

        });
        Schema::table('galleries', function (Blueprint $table) {
            $table->foreignId('department_id')->nullable()->constrained();
            $table->foreignId('university_calender_id')->nullable()->constrained();

        });
        Schema::table('gallery_users', function (Blueprint $table) {
            $table->foreignId('user_id')->nullable()->constrained();
            $table->foreignId('gallery_id')->nullable()->constrained();

        });
        Schema::table('years', function (Blueprint $table) {
            $table->foreignId('college_id')->nullable()->constrained();


        });
        Schema::table('generations', function (Blueprint $table) {
            $table->foreignId('college_id')->nullable()->constrained();


        });
        Schema::table('generation_years', function (Blueprint $table) {
            $table->foreignId('year_id')->nullable()->constrained();
            $table->foreignId('generation_id')->nullable()->constrained();
            $table->foreignId('department_id')->nullable()->constrained();
            $table->foreignId('university_calender_id')->nullable()->constrained();


        });
        Schema::table('generation_user_years', function (Blueprint $table) {
            $table->foreignId('generation_year_id')->nullable()->constrained();
            $table->foreignId('user_id')->nullable()->constrained();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collegekeys');
    }
}
