<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUniversitykeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('semesters', function (Blueprint $table) {
            $table->foreignId('university_id')->nullable()->constrained();
        });
        Schema::table('specializations', function (Blueprint $table) {
            $table->foreignId('specialization_type_id')->nullable()->constrained();

        });
        Schema::table('university_calenders', function (Blueprint $table) {
            $table->foreignId('university_id')->nullable()->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universitykeys');
    }
}
