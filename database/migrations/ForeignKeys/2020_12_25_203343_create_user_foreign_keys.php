<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->foreignId('user_type_id')->constrained();
           $table->foreignId('college_id')->constrained();
           $table->foreignId('university_id')->constrained();
        });

        Schema::table('saves', function (Blueprint $table) {
            $table->foreignId('post_id')->constrained();
            $table->foreignId('category_save_id')->constrained();
        });

        Schema::table('category_saves', function (Blueprint $table) {
            $table->foreignId('user_id')->constrained();
        });

        Schema::table('user_tokens', function (Blueprint $table) {
            $table->foreignId('user_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
