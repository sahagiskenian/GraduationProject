<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialForeginKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conversations', function (Blueprint $table) {
            $table->foreignId('sender_id')->nullable()->constrained('users','id');
            $table->foreignId('receiver_id')->nullable()->constrained('users','id');
            $table->foreignId('generation_id')->nullable()->constrained();
        });

        Schema::table('conversation_messages', function (Blueprint $table) {
            $table->foreignId('conversation_id')->nullable()->constrained();
            $table->foreignId('sender_id')->nullable()->constrained('users','id');
        });

        Schema::table('conversation_message_receivers', function (Blueprint $table) {
            $table->foreignId('conversation_message_id')->nullable()->constrained();
            $table->foreignId('receiver_id')->nullable()->constrained('users','id');
        });

        Schema::table('conversation_group_users', function (Blueprint $table) {
            $table->foreignId('conversation_id')->constrained();
            $table->foreignId('user_id')->constrained();
        });

        Schema::table('conversation_group_headers', function (Blueprint $table) {
            $table->foreignId('conversation_id')->constrained();
        });

        Schema::table('tabs', function (Blueprint $table) {
            $table->foreignId('university_id')->nullable()->constrained();
            $table->foreignId('college_id')->nullable()->constrained();
            $table->foreignId('generation_year_id')->nullable()->constrained();
            $table->foreignId('specialization_id')->nullable()->constrained();
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->foreignId('tab_id')->nullable()->constrained();
            $table->foreignId('user_id')->nullable()->constrained();
            $table->foreignId('generation_user_year_id')->nullable()->constrained();
            $table->foreignId('subject_id')->nullable()->constrained();
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->foreignId('post_id')->nullable()->constrained();
            $table->foreignId('user_id')->nullable()->constrained();
            $table->foreignId('generation_user_year_id')->nullable()->constrained();
            $table->foreignId('comment_id')->nullable()->constrained();
        });

        Schema::table('reacts', function (Blueprint $table) {
            $table->foreignId('post_id')->constrained();
            $table->foreignId('comment_id')->constrained();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('generation_user_year_id')->constrained();
        });

        Schema::table('ratings', function (Blueprint $table) {
            $table->foreignId('post_id')->constrained();
            $table->foreignId('generation_user_year_id')->constrained();
            $table->foreignId('comment_id')->constrained();
            $table->foreignId('user_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
