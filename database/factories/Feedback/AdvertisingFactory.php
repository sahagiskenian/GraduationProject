<?php

namespace Database\Factories\Feedback;

use App\Models\Feedback\Advertising;
use Illuminate\Database\Eloquent\Factories\Factory;

class AdvertisingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Advertising::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'image_url'=> $this->faker->imageUrl(300,300,'dogs'),
            'link'=>$this->faker->url
        ];
    }
}
