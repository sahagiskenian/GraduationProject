<?php

namespace Database\Factories\University_Common;

use App\Models\University_Common\UniversityCalender;
use Illuminate\Database\Eloquent\Factories\Factory;

class UniversityCalenderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UniversityCalender::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'image' => $this->faker->imageUrl($width = 640, $height = 480),
            'start_date' => $this->faker->numberBetween(2016,2026) ,
            'end_date' =>$this->faker->numberBetween(2016,2026)  ,
            'university_id'=>$this->faker->numberBetween(6,10),

        ];
    }
}
