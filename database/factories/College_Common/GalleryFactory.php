<?php

namespace Database\Factories\College_Common;

use App\Models\College_Common\Gallery;

use Illuminate\Database\Eloquent\Factories\Factory;

class GalleryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Gallery::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name() ,
            'description' => $this->faker->paragraph(3,true)  ,
            'logo'=>$this->faker->imageUrl($width = 640, $height = 480),
            'department_id'=>$this->faker->numberBetween(1,5),
            'university_calender_id'=>$this->faker->numberBetween(1,5)

        ];
    }
}
