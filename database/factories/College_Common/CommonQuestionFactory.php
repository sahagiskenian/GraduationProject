<?php

namespace Database\Factories\College_Common;

use App\Models\College_Common\CommonQuestion;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommonQuestionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CommonQuestion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'answer' => $this->faker->sentence($nbWords = 6, $variableNbWords = true) ,
            'question' => $this->faker->sentence($nbWords = 6, $variableNbWords = true)  ,
            'college_id'=>$this->faker->numberBetween(6,10),

        ];
    }
}
