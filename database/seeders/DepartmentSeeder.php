<?php

namespace Database\Seeders;

use App\Models\College_Common\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Department::insert([
            [
                'name' => 'AI',
                'college_id'=>6,
            ] ,
             [
                'name' => 'Geometric',
                'college_id'=>7,
             ] ,
             [
                'name' => 'Networking',
                'college_id'=>6,
             ] ,
             [
                'name' => 'Programming',
                'college_id'=>6,
             ] ,
             [
                'name' => 'example',
                'college_id'=>7,
             ] ,
         ]);
    }
}
