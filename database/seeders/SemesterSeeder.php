<?php

namespace Database\Seeders;

use App\Models\University_Common\Semester;
use App\Models\University_Common\University;
use Illuminate\Database\Seeder;

class SemesterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach(University::all() as $university){
            Semester::insert([
                [
                    'semester' => 1,
                    'university_id'=>$university->id,
                ] ,
                 [
                    'semester' => 2,
                    'university_id'=>$university->id,
                 ],

             ]);
        }
    }
}
