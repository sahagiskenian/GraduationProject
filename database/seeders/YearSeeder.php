<?php

namespace Database\Seeders;

use App\Models\College_Common\College;
use App\Models\College_Common\Year;
use Illuminate\Database\Seeder;

class YearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(College::all() as $college){
            Year::insert([
                [
                    'name' => 1,
                    'college_id'=>$college->id,
                ] ,
                 [
                    'name' => 2,
                    'college_id'=>$college->id,
                 ] ,
                 [
                    'name' => 3,
                    'college_id'=>$college->id,
                 ] ,
                 [
                    'name' => 4,
                    'college_id'=>$college->id,
                 ] ,
                 [
                    'name' => 5,
                    'college_id'=>$college->id,
                 ] ,
             ]);
        }

    }
}
