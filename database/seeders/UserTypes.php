<?php

namespace Database\Seeders;

use App\Models\User\UserType;
use Illuminate\Database\Seeder;

class UserTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserType::insert([
           [
               'name' => 'superAdmin'
           ] ,
            [
                'name' => 'university_admin'
            ] ,
            [
                'name' => 'college_admin'
            ] ,
            [
                'name' => 'generation_admin'
            ] ,
            [
                'name' => 'student'
            ] ,
        ]);
    }
}
