<?php

namespace Database\Seeders;

use App\Models\University_Common\SpecializationType  ;
use Illuminate\Database\Seeder;

class specializationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SpecializationType::insert([
            [
                'name' => 'Computer Science ',

            ] ,
             [
                'name' => 'Medicine ',

             ],

         ]);
    }
}
