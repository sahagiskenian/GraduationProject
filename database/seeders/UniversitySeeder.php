<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\University_Common\University;
class UniversitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        University::insert([
            [
                'name' => 'Aleppo'
            ] ,
             [
                 'name' => 'Damascus'
             ] ,
             [
                 'name' => 'Tshreen'
             ] ,
             [
                 'name' => 'Furat'
             ] ,
             [
                 'name' => 'Ittihad Private'
             ] ,
         ]);
    }
}
