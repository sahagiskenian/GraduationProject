<?php

namespace Database\Seeders;

use App\Models\University_Common\Specialization;
use Illuminate\Database\Seeder;

class SpecializationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Specialization::insert([
            [
                'name' => 'Communication ',
                'specialization_type_id'=> 1,

            ] ,
             [
                'name' => 'Pharmacy ',
                'specialization_type_id'=> 2,
             ],

         ]);
    }
}
