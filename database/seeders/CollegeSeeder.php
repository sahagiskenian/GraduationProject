<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\College_Common\College;
class CollegeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        College::insert([
            [
                'name' => 'IT',
                'university_id'=>6,
            ] ,
             [
                 'name' => 'Architect',
                 'university_id'=>7,
             ] ,
             [
                 'name' => 'History',
                 'university_id'=>6,
             ] ,
             [
                 'name' => 'Math',
                 'university_id'=>8,
             ] ,
             [
                 'name' => 'Biology',
                 'university_id'=>6,
             ] ,
         ]);
    }
}
